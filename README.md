# Index

[Presentazione](wiki/notes/README.md)

[Directory del progetto](wiki/notes/Directory del progetto.md)

[Panoramica](wiki/notes/Panoramica.md)

[Basi di partenza](wiki/notes/Basi di partenza.md)

[Punti di discussione](wiki/notes/Punti di discussione.md)

---

[Concetti di base](wiki/notes/Concetti di base.md)

[Variabili impiegate nel programma](wiki/notes/Variabili impiegate nel programma.md)

---

[Logger e debugging](wiki/notes/Logger e debugging.md)

[Pannello statistiche](wiki/notes/Pannello statistiche.md)

[Salvataggio dati su file](wiki/notes/Salvataggio dati su file.md)

---

[Studiare il modello](wiki/notes/Studiare il modello.md)

[Sull'Automa Cellulare e TrafficSimulator](wiki/notes/Sull'automa Cellulare e TrafficSimulator.md)

---

[Ripartizione dei compiti](wiki/notes/Ripartizione dei compiti.md)

---

### Relazione del progetto

[Tavola dei contenuti](wiki/notes/Tavola dei contenuti.md)

[Analisi - Capitolo 1](wiki/notes/Capitolo 1 - Analisi.md)

[Design - Capitolo 2](wiki/notes/Capitolo 2 - Design.md)

### Linee guida

[Git](wiki/notes/Git.md)

[Gradle](wiki/notes/Gradle)

[Qualità del software](wiki/notes/Qualità del software.md)

[Qualità del codice](wiki/notes/Qualità del codice.md)

[Configurazione locale di Eclipse](wiki/notes/Configurazione locale di Eclipse.md)

[Convezioni nella scrittura del codice](wiki/notes/Convenzioni sulla scrittura del codice.md)

[Strumenti, librerie e fonti impiegate](wiki/notes/Strumenti, librerie e fonti impiegate.md)
