---
title: Gerarchie di classi e builder
created: '2020-02-29T10:23:37.134Z'
modified: '2020-02-29T10:35:10.278Z'
---

# Gerarchie di classi e builder

Si sfrutta una tecnica che impiega la keyword this e self() per poter implementare a partire da un builder astratto dei builder per specifiche sottoclassi.

> **Nota:** nel seguente codice manca il controllo d'errore con relativo lancio di eccezioni, e manca la documentazione. Va preso come esempio, dato che anche la realizzazione del builder è diversa da quella che ci è stata presentata al corso.

<details>
<summary>  Applicazione del pattern builder per gerarchie di classi </summary>

~~~java
import java.util.*;

// Pattern builder per gerarchie di classi

// Note that the underlying "simulated self-type" idiom  allows for 
// arbitrary fluid hierarchies, not just builders

public abstract class Pizza {
    public enum Topping { HAM, MUSHROOM, ONION, PEPPER, SAUSAGE }
    final Set<Topping> toppings;

    abstract static class Builder<T extends Builder<T>> {
        EnumSet<Topping> toppings = EnumSet.noneOf(Topping.class);
        public T addTopping(Topping topping) {
            toppings.add(Objects.requireNonNull(topping));
            return self();
        }

        abstract Pizza build();

        // Subclasses must override this method to return "this"
        protected abstract T self();
    }
    
    Pizza(Builder<?> builder) {
        toppings = builder.toppings.clone(); // See Item 50
    }
}
~~~

</details>

<details>
<summary> Sottoclasse con builder gerarchico con enum </summary>

~~~java
import java.util.Objects;

// Subclass with hierarchical builder (Page 15)
public class NyPizza extends Pizza {
    public enum Size { SMALL, MEDIUM, LARGE }
    private final Size size;

    public static class Builder extends Pizza.Builder<Builder> {
        private final Size size;

        public Builder(Size size) {
            this.size = Objects.requireNonNull(size);
        }

        @Override public NyPizza build() {
            return new NyPizza(this);
        }

        @Override protected Builder self() { return this; }
    }

    private NyPizza(Builder builder) {
        super(builder);
        size = builder.size;
    }

    @Override public String toString() {
        return "New York Pizza with " + toppings;
    }
}

~~~

</details>

<details>
<summary> Sottoclasse con builder gerarchico con possibilità di specificare un parametro booleano  </summary>

~~~java
// Sottoclasse con builder applicato a classi gerarchiche
public class Calzone extends Pizza {
    private final boolean sauceInside;

    public static class Builder extends Pizza.Builder<Builder> {
        private boolean sauceInside = false; // Default

        public Builder sauceInside() {
            sauceInside = true;
            return this;
        }

        @Override public Calzone build() {
            return new Calzone(this);
        }

        @Override protected Builder self() { return this; }
    }

    private Calzone(Builder builder) {
        super(builder);
        sauceInside = builder.sauceInside;
    }

    @Override public String toString() {
        return String.format("Calzone with %s and sauce on the %s",
                toppings, sauceInside ? "inside" : "outside");
    }
}
~~~

</details>

</details>
