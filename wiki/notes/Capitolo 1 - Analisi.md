---
tags: [Relazione]
title: Capitolo 1 - Analisi
created: '2020-03-01T09:35:13.790Z'
modified: '2020-03-24T11:25:05.489Z'
---

# Capitolo 1 - Analisi

## Requisiti

Il software che abbiamo sviluppato si pone l'obiettivo di realizzare una simulazione semplificata di scenari di traffico.

L'utente può controllare:
  - la **frequenza delle macchine** che provengono da ogni direzione;
  - la **velocità** delle macchine;
  - la **tempistica del semaforo** all'intersezione.

In base allo scenario scelto l'utente potrà comprendere come ciascuna configurazione stradale è in grado di gestire flussi di traffico diversi.

### Requisiti funzionali

- semaforo in grado di alterare il suo comportamento a run-time in base all'input dell'utente (dinamico che cambia durata delle fasi in base al traffico)

- diverse tipologie di veicoli il cui afflusso nello scenario viene determinato a run-time in base all'input dell'utente

- possibilità di creare diversi scenari (dettaglio implementativo: in base a parametri specificati in un file)

- possibilità di scegliere tra vari scenari

**GUI**

- wizard per caricare uno scenario (intersezione-1-corsia)

- barra del menù per varie configurazioni

- finestra apposita per statistiche aggiornate a run-time

- pannello gestione del tempo e di configurazione a disposizione dell'utente per avviare e modificare i parametri della simulazione a run-time.

### Requisiti non funzionali

- internazionalizzazione, portabilità ( apertura in base alla dimensione dello schermo e ridimensionabilità a run-time)

- mantenimento di un log

- mantenimento di un archivio di statistiche salvate dall'utente, oltre ad un record e ai dati sulle ultime statistiche raccolte (dettaglio implementativo: la cartella archivio viene generata automaticamente se non presente)

## Analisi del modello e del dominio

Lo stato iniziale dell'automa dovrà essere modificabile alterando il comportamento delle entità coinvolte nella simulazione tramite un apposito pannello di controllo. L'utente è in grado di interpretare l'andamento della simulazione sia grazie all'output grafico che alle statistiche.

L'automa aggiorna il proprio stato ad ogni iterazione in seguito ad una computazione che avviene in base 


























