---
title: Sull'Automa Cellulare e TrafficSimulator
created: '2020-02-23T20:23:03.969Z'
modified: '2020-03-24T11:25:30.936Z'
---

# Sull'Automa Cellulare e TrafficSimulator

[[Index|Index.md]]

---

Tratto da: [Wikipedia](https://www.wikiwand.com/it/Automa_cellulare)

---
## Sull'automa cellulare (AC)

Un automa cellulare è un modello matematico usato per descrivere l'evoluzione di sistemi complessi discreti.

Un automa cellulare consiste di una griglia costituita da celle.

La griglia può avere una qualunque dimensione finita; ogni porzione limitata di spazio deve contenere solo un numero finito di celle. Ciascuna di queste celle può assumere un insieme finito di stati.

Per ogni cella è necessario anche definire l'insieme delle celle che sono da considerare "vicine" alla cella data.

Ad un certo tempo t=0 si assegna ad ogni cella un determinato stato. L'insieme di questi stati costituisce lo stato iniziale dell'automa cellulare.

Il modo in cui cambia stato una cella dipende solamente dal proprio stato attuale e dagli stati delle celle "vicine". 

Ogni AC possiede gli stessi elementi: un insieme di posizioni connesse, un insieme di stati in ogni posizione e regole di aggiornamento per lo stato delle posizioni.

È possibile definire in modo formale gli automi cellulari tenendo conto di tre caratteristiche fondamentali:

  - la **rappresentazione spaziale** delle entità coinvolte.
  - l'**uniformità**: le entità che si trovano in ciascun punto dello spazio sono identiche.
  - la **località**: ogni entità cambia stato tenendo conto solamente di quanto succeda entro una certa distanza.

Gli automi cellulari sono adatti a rappresentare e simulare l'evoluzione globale di fenomeni che dipendono solo da leggi locali.

Secondo la classificazione di Wolfram, automi cellulari interessanti, tra cui il gioco della vita, sono di classe 4. **Questo automa cellulare che stiamo sviluppando di che classe è?**, Se appartiene alla classe 1 o 2 vuol dire che è scarsamente interessante in quanto altamente prevedibile e poco caotico. Dovremmo preoccuparci di questa classificazione perché determina quanto è interessante il nostro progetto.

**Il nostro AC gode della proprietà della Reversibilità?** In altri termini il suo comportamento a tempo inverso può essere ancora descritto come un automa cellulare? In generale rispondere a questa domanda **non** ci interessa.

---

Tratto da: [*Introduction to the Theory of Cellular Automata and One-Dimensional Traffic Simulation* by Richard Cochinos](https://theory.org/complexity/traffic/)

---

## Introduzione

Vogliamo creare un modello che simuli un sistema di traffico di dimensioni ridotte.
(Rappresenta un esempio concreto di simulazione in 2D di Automa Cellulare, si tratta di modellare
il comportamento umano in uno spazio finito chiuso)

In particolare vogliamo creare un modello che descriva il comportamento come risultante dall'*interazione reciproca di entità discrete* (modello microscopico).

Il principale limite di questo tipo di modello sta nel considerare il comportamento di ogni veicolo come uniforme. (non si riesce a modellare il comportamento di ciascun automobilista). 

## Un possibile modello

Il modello di traffico da definire è una **griglia di LxL celle** con *bordi chiusi* (non si sconfina nel lato opposto).

Il numero totale di veicoli N nel sistema è costante ed è definito da appositi parametri *memorizzati in appositi file*.

~~~Java
/* Si specifica il numero di veicoli per ciascuna strada */
NORD_CARS = 10;
EST_CARS = 10;
/* Si ottiene N */
TOT_CARS = NORD_CARS + EST_CARS;
~~~

Alcune regole comuni a tutti gli scenari:

- Ogni cella può essere occupata al massimo da un veicolo oppure essere vuota. (niente scontri) 
- Ad ogni cella corrisponde un segmento di strada di lunghezza $l$. 
- La **densità del traffico globale** (a livello dell'intero scenario) è data dalla seguente formula: $$\mu=\frac{N}{L}$$ si possono poi definire le densità per ciascuna strada definendo $x$ parametri $N_i$ dove $x$ indica il numero di strade e $N_i$ il numero di macchine dell'i-esima strada.

- Ogni macchina ha una velocità che oscilla tra 0 e VMAX. (definita nel *pannello di configurazione*) 
- La velocità corrisponde al numero di celle di cui avanza un'auto in un'iterazione. (solo celle valide) 
- Il **movimento dei veicoli** attraverso le celle è definito da un insieme di regole di aggiornamento. 

<details>
<summary> Regole che definiscono il movimento dei veicoli </summary>

  - Queste regole sono applicate **in parallelo, a ciascun veicolo e ad ogni iterazione**.
  - La **durata di un'iterazione** può essere scelta per riflettere il livello di dettaglio nella simulazione desiderato.
  - Si può scegliere perciò un'iterazione di lunghezza sufficientemente piccola per approssimare un sistema temporale continuo. (sistema in cui il tempo è entità continua e non discreta, in altri termini se un iterazione avviene ogni secondo piuttosto che ogni 10 si riesce a creare l'illusione di macchine in continuo movimento molto meglio) 
  - Lo **stato del sistema ad una data iterazione** è determinato dalla distribuzione dei veicoli tra le celle e dalla velocità di ciascun veicolo in ogni cella. (per *debug*, *statistiche* e *salvataggio su file*)
   
  - Si impiega la seguente **notazione**:

    - $p(i)$: posizione dell'i-esimo veicolo (*Pair* di coordinate cartesiane)
    - $v(i)$: velocità dell'i-esimo veicolo (intero compreso tra 0 e VMAX)
    - $g(i)$: distanza tra l'i-esimo veicolo e il successivo $(i+1)$, data da: $g(i)=p(i+1)-p(i)$ (intero)

Per questo modello si adotta il seguente insieme di regole:

1) **Accelerazione** di un *veicolo libero* (non ostacolato da altri automobilisti): $$v(i+1)=v(i)+1$$
**NB:** La relazione è valida solo se $v(i)<v_{max}$ e $g(i)>v(i)+1$. (può essere visto come il *delta positivo* della velocità)

2) **Rallentamento** dovuto ad altri veicoli:  $$v(i)=g(i)$$
**NB:** La relazione è valida solo se $v(i)>g(i)-1$. (può essere visto come il *delta negativo* della velocità)

3) **Avanzamento** del veicolo: il veicolo si sposta di un numero di celle pari alla sua velocità $v(i)$.(rappresenta il *delta di spostamento*)

</details>

## Varianza della velocità e flusso massimo del traffico

La **varianza della velocità** aumenta notevolmente mano a mano che il sistema si avvicina al raggiungimento del **flusso massimo**, che costituisce l'obiettivo della maggior parte dei piani di traffico.

Un'**elevata varianza della velocità** indica che distinti veicoli nel sistema hanno velocità che variano notevolmente. Ciò significa inoltre che ciascun veicolo in ciascun viaggio nel sistema è soggetto a **frequenti cambi di velocità**.

Di conseguenza si ha una **maggiore variabilità nei tempi di viaggio**, ovvero nei tempi di attraversamento del sistema. Questo aumenta le probabilità di incidenti (che comunque non consideriamo per ora).

Perciò, per quanto possa sembrare controintuitivo, sarebbe meglio fare in modo che il traffico non raggiunga la massima portata del sistema, o in altri termini, che raggiunga il flusso massimo.

---

## Di che si tratta? Un piccolo approfondimento...

Questo modello impiega il concetto di Automa Cellulare.

In particolare si tratta di un modello di Automi Cellulari **deterministico, reversibile e finito** con confini periodici. Di conseguenza il relativo sistema di traffico è periodico negli stati che assume, in altri termini *il numero di iterazioni necessarie a ripristinare lo stesso stato è sempre finito*. 
(tra le altre cose significa che azioni come "torna indietro" e "torna avanti" possono essere implementate facilmente)

Gli Automi Cellulari deterministici sono utili come paradigma per la modellazione di sistemi autostradali automatizzati, in cui l'accelerazione e la decelerazione dei veicoli è controllata dall'esterno.

Infatti un sistema autostradale funzionerebbe in modo molto simile ad un modello di AC deterministico con regole che specificano il **numero totale di veicoli ammessi nel sistema** e un **insieme di comportamenti dei veicoli ammessi**.

L'impiego di Automi Cellulari presenta alcuni vantaggi:

L'informazione che portano non è presentata in modo astratto, l'output di un automa cellulare è in forma visuale e perciò si può comprendere più facilmente quali sono le dinamiche che animano il modello.
