---
title: 'Strumenti, librerie e fonti impiegate'
created: '2020-02-25T08:12:44.383Z'
modified: '2020-03-24T11:25:31.026Z'
---

# Strumenti, librerie e fonti impiegate

[[Index|Index.md]]

---

## Strumenti 

- Per la creazione dei wireframes: **WireframeSketcher** (licenza di prova per 14 giorni, valida per computer e non per persona)
- Per la relazione in Latex: **Overleaf** - versione Desktop (richiede creazione account, volendo si può usare quello istituzionale)
- Per comunicazione: **Discord**
- Per la gestione del wiki (file Markdown): **Notable** (si potrebbe usare anche l'editor online di Bitbucket o altri editor di testo come Visual Studio Code,ecc.)
- Per il debugging **JVisualVM** (forse JConsole).
- Per la gestione delle dipendenze: **Gradle**
- Per lo sviluppo dell'applicazione: **Eclipse IDE**, con alcuni plugin:
  
  *obbligatori*
  - **Checkstyle Plug-in 8.29.0**
  - **pmd-eclipse-plugin 4.10.0**
  - **SpotBugs Eclipse plugin 3.1.5.r201806132012-cbbf0a5**
  - Buildship Gradle Integration 3.0
  
  *facoltativi*
  - e(fx)clipse 3.6.0
  - Eclipse TeXlipse 2.0.2, per usare Latex in Eclipse, non in uso al momento
  - Eclipse Xtend 2.20.0 (già presente in Eclipse)
  - EGit - GIt integration for Eclipse 5.5.1 (già presente in Eclipse)
  - Object Aid UML Explorer, permette di creare diagrammi di classe UML in Eclipse, direttamente a partire dal codice [[https://objectaid.com/class-diagram]]
  ~~- WindowBuilder 1.9.2~~
  ~~- Yaml Editor 1.6.2~~

## Librerie

**NB:** con l'impiego di Gradle sarebbero inserite automaticamente nel classpath.

- **Google Guava** per la concorrenza (forse);
- **Apache Commons IO** per gestire le operazioni di I/O; [[https://commons.apache.org/proper/commons-io/]]
- **SLF4J** per il logging; [[http://www.slf4j.org/docs.html]]
- **JUnit 5** per i test;
- **JavaFx** per la GUI.

## Fonti

- Dispense del corso di OOP,
- stackoverflow,
- modelingCommons,
...


