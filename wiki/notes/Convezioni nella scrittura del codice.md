---
title: Convezioni nella scrittura del codice
created: '2020-02-25T13:58:15.916Z'
modified: '2020-03-24T11:25:31.706Z'
---

# Convezioni nella scrittura del codice

[[Index|Index.md]]

---

- **Convenzioni sulle parentesi**

Usare sempre le parentesi per if, else, for, while,anche se segue una sola istruzione

Le parentesi graffe vanno sempre “all’egiziana”

- **Convenzioni sui nomi**

- I nomi di package usano sempre e solo lettere minuscole
- Usare sempre camelCase, evitare gli underscore ( )
- I nomi di classe cominciano sempre per maiuscola
- I nomi di campi, metodi e variabili locali iniziano sempre per minuscola
- I campi static final (costanti di classe) sono interamente maiuscoli e possono usare underscore.
