---
title: Concetti di base
created: '2020-02-24T20:02:10.754Z'
modified: '2020-03-24T11:25:31.801Z'
---

# Concetti di base

[[Index|Index.md]]

---

## Competenze richieste da questo progetto

Capacità di tradurre una descrizione in linguaggio naturale di una applicazione in termini di astrazioni adatte ad essere scritte in forma di software.

(nel nostro caso, in elementi di linguaggio di programmazione orientato agli oggetti)

In altri termini capacità di analisi del problema e di design della soluzione.

È prioritario impostare il design dell'applicazione su carta e penna prima di iniziare a scrivere il codice.

---

#### Cos'è un wizard? 

Un **wizard** permette di eseguire determinate operazioni tramite una serie di passi successivi. Semplifica l'esecuzione di una procedura che altrimenti sarebbe più complessa.

In generale un wizard:
- è semplice da usare;
- limita le scelte che deve effettuare l'utente per favorire la semplicità d'uso;
- permette di eseguire operazioni che devono essere eseguibili nell'applicazione anche in altro modo;
- permette di tornare indietro per modificare le proprie scelte;
- impiega un'interfaccia grafica.

Per ulteriori informazioni consultare: [[https://www.wikiwand.com/it/Wizard_(informatica)]]

#### In questo programma a cosa serve un wizard?

Per semplificare la configurazione iniziale dell'ambiente simulativo. In particolare per stabilire il valore con cui inizializzare alcune variabili (relative per esempio allo scorrere del tempo, come la durata di un'iterazione) e determinare lo scenario (intersezione a 1 corsia, a 2, ecc.), oltre che eventuali condizioni particolari (meteo, tipologia di veicoli, ostacoli, ecc.)

**NB:** nel wireframe il wizard è ancora incompleto (si trova nello schermata di Avvio)

**Dettaglio implementativo:** Un wizard può essere implementato con il **pattern Command** [[https://www.wikiwand.com/en/Command_pattern]].

#### Cos'è un wireframe? 

Un **wireframe** rappresenta un *progetto della GUI* e uno **storyboard** raccoglie i singoli **schermi** che insieme formano l'interfaccia messa a disposizione dal programma nei confronti dell'utente.

Un wireframe permette di condividere e organizzare le idee con maggior chiarezza e di limitare i fraintendimenti.

- [ ] Il concetto di **iterazione** in relazione a quello di **istante di tempo** va chiarito.

**NB:** per convenzione i nomi in maiuscolo indicano delle costanti, perciò **non va bene** indicare le variabili in questo modo a meno che non si tratti di valori iniziali, ovvero di costanti.
