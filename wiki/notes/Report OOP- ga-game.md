---
title: Report OOP- ga-game
created: '2020-03-31T12:33:58.858Z'
modified: '2020-04-01T08:59:51.345Z'
---

# Report OOP- ga-game

Il fulcro del gioco è la simulazione di un mondo bidimensionale ~~con le proprie leggi fisiche~~ e le interazioni fra le molteplici entità che lo compongono allo scorrere del tempo. Tutte le entità posseggono quindi un corpo individuato da una **posizione** ed una **dimensione** all’interno di tale mondo.

Dal momento che possono muoversi, entrare in contatto ed influenzarsi a vicenda, i corpi devono poter rispondere a stimoli (impulsi) esterni. Le entità sono ulteriormente caratterizzate dalla presenza (o assenza) di una serie di macro componenti.


GameWorld, EntityController, GameWorldView

Il controller inizialiazza e coordina il model ed aggiorna la view di conseguenza. La gestione delle Entity viene delegata a singoli EntityController specializzati che aggiornano la relativa EntityView in base allo stato del model.

Perché sia agevole il cambio di view occorre definire dei componenti generici della view che devono poi essere specificati con moduli di JavaFx?

Le parti in comune sono state racchiuse in classi astratte (Template Method per il riuso di codice)

AbstractMovement definisce la logica di movimento delle macchine in due metodi: `computeMovement` and `applyMovement`, rispettivamente per gestire il calcolo del movimento da effettuare e la sua effettiva applicazione in base allo stato corrrente dell'ambiente simulativo.

`EntityController` ed `EntityComponents` disaccoppiati con architettura publish-subscribe. Gli EntityComponents impiegano questo meccanismo per pubblicare cambiamenti di stato tramite `EntityEvents`. (eventbus di Guava)

Creato wrapper per il motore della fisica Box2D. Intercam

*Predicate, Supplier and Cousumer and Operators ???*

`EntityController`, con sue specializzazioni, aggiorna la view tramite eventi con informazioni relative all'HUD (durata fasi semaforiche in sovraimpressione), oltre che informazioni per le statistiche.

Si impiega una classe di view detta `HudView`.

PlayerView data da `LivingEntityView` e `HudView`. Sia a livello di interfacce che di Implementazione. L'implementazione di `LivingEntityView` è data da una classe astratta.

Alla LivingEntityView è associato un `LivingEntityController` (interfaccia e implementazione)

`PlayerController` eredita da `LivingEntityController` e realizza il contratto di `Entity` (che sfrutta `Movement`)

---

In questa sezione tratterò di come viene caricato un livello assemblandone i
componenti, i menù, il comparto audio, lingue e la gestione dei progressi di
gioco e opzioni.

Il ”cuore” dell’applicazione è composto dal MainController, che coordina il funzionamento dell’applicazione attraverso il pattern Observer, gestendo tutti i controller specifici e il MainView. 

A sua volta, col medesimo pattern, il MainView gestisce tutte le view specifiche. Tutte le schermate hanno un’architettura MVC con interfacce. 

Alla richiesta di un azione da parte di un sotto-controller o all’intercettazione di un evento, il MainController gestisce la richiesta.

**Un livello è composto da mappa ed entità.**

La mappa contiene informazioni sulla posizione e la natura delle entità, su ciò che è solido e la veste grafica, il caricamento del livello avviene tramite la lettura di queste informazioni attraverso `LoadLevel`. 

Viene prima caricata nel `GameWorldView` la parte grafica, successivamente vengono create le zone tangibili, infine caricate le entità. 

Per caricare le entità ne viene letta la posizione, poi viene letta la natura dell’entità, a ciascun type corrisponde un’entità diversa. 

Col pattern Factory viene generata l’entità richiesta e collocata nelle coordinate corrispondenti.

Le lingue sono gestite dalla classe `LoadLanguage`, in base alla lingua scelta dalle opzioni tutto il testo presente nel gioco viene tradotto di conseguenza. 

La gestione dei progressi di gioco e opzioni avviene tramite `MainController`, che salva e carica usando la classe `SaveLoadManager`, che registra progressi e opzioni attuali. `I salvataggi di gioco e opzioni sono modellati come oggetti con dei campi.`

---

Da `AbstractEntityController` si hanno `MovingEntityController` e `StaticEntityController`, si hanno poi `VehicleController` e `TrafficLightController` e `RoadController`.

Le `Rules` sono dei componenti con lo scopo di gestire il comportamento delle entità a cui sono collegati rispetto alle entità con cui entrano in contatto nel mondo di gioco.

Triggerable e Trigger, Collisionable e Collision, Timeable e Timer, le classi interassate a queste funzionalità estendono e si mettono in ascolto di messaggi o rendono altre entità/classi in grado di ascoltare messaggi da loro.

Per fare in modo che dei componenti `Trigger` e `Triggerable` comunichino, vengono associate loro delle password per collegare lo scatenamento degli eventi inviati dal `Trigger` all’attivazione dei `Triggerable` a cui è stata associata la stessa password dell’evento. 

In tal caso, gli eventi inviati dai Trigger vengono ascoltati da un classe `TriggerLinker`, che si occuperà di attivare i componenti `Triggerable` collegati; a loro volta, anche questi invieranno un evento di attivazione.

Il pattern Strategy è utilizzato anche dal componente FixedPatternPilot, che prende in input un Supplier di punti bidimensionali: essi determinano il percorso che il corpo dell’entità seguirà attraverso l’utilizzo dell’eventuale componente Movement.

Per quanto riguarda le librerie esterne, ho fatto uso della classe EventBus della libreria Google Guava per gestire l’invio e l’ascolto di eventi tra classi senza che siano collegate esplicitamente tra di loro.



