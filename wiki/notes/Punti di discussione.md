---
title: Punti di discussione
created: '2020-02-29T14:27:15.505Z'
modified: '2020-03-24T11:24:23.287Z'
---

# Punti di discussione

objectRequireNonNull

Collections.unmodifiable

final

#### Sinergia di Template Method e Strategy

Definisce lo scheletro (template) di un algoritmo (o comportamento), lasciando l’indicazione di alcuni suoi aspetti alle sottoclassi.

Definisce una famiglia di algoritmi, e li rende interscambiabili, ossia usabili in modo trasparente dai loro clienti.



#### Classi astratte o decorator?

Decorator è un pattern che permette di variare dinamicamente le responsabilità di un oggetto senza ricorrere alla creazione di sottoclassi.

(i.e. creare un modulo per il raggio d'azione di un veicolo, oppure meteo/ostacoli/pendenza rispetto alla strada)


#### un oggetto == una cella?

Conviene davvero istanziare un veicolo oppure bisogna tenere il conto di tutte le macchine presenti in una struttura dati apposita?

I thread permettono di gestire una macchina o più macchine, un gruppo preciso di macchine? Tutte le macchine?

#### Concetto di responsabilità di classi e oggetti

#### Meglio impiegare un'interfaccia piuttosto che una classe astratta

Per le interfacce vale l'ereditarietà multipla

### Riuso del codice

La parametrizzazione dei tipi, l'ereditarietà delle classi e la composizione di oggetti (di cui la delegazione è esempio estremo) forniscono tre modi per comporre il comportamento in sistemi OO.


#### Meglio la composizione che l'ereditarietà

La delegazione è una strategia di riuso migliore in molti contesti.
L'ereditarietà anche se mitigata dall'impiego di classi astratte fissa i legami tra classi a compile-time. Noi vorremmo invece che fossero modificabili a run-time (come rende possibile la composizione)

#### Impiego dei generici

Implementazioni concrete -> tipi generici

Lavorare con tipi quanto più generici possibile e impiegare dei mapper per rendere le classi definite a livello generale in implementazione specifiche ad un tipo (vedere arkanoid e la classe Mapper)

#### Eden

Non si devono modificare classi precedentemente create, si devono solo aggiungere nuove implementazioni senza codice ripetuto, senza cose che non servono e aggiungendo solo quelle che servono.

Gestione dei diversi tipi di veicoli
gestione dei diversi tipi di semafori
gestione dei set di regole
strada builder espanso con composizione
griglia ?

- niente factory per la griglia

createScenarios.setName("intersection").enableWeather("fog","rain","heavy rain").enableObstacles("bumpy road","work in progress").setColumns(80).setRows(100)/.setSquare(100).setBackgroundColor(Colors.GREEN).build();

- niente factory per le strade

streetsBuilder.setName().setSource(x,y).setSink(x,y).setColor(Colors.GREY).build();

- factory per i vari tipi di veicoli e builder per la costruzione di ciascuno di essi

createVehicle.car().setSource(streetName.getSource()).setRules(Extended).setVelocity(2).setAcceleration(1).setSightRange(1).setColor(randomColor).build()
