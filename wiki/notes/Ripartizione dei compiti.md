---
title: Ripartizione dei compiti
created: '2020-02-24T20:31:55.003Z'
modified: '2020-03-24T11:25:31.127Z'
---

# Ripartizione dei compiti

[[Index|Index.md]]

---

- **Adrian Lazar**: 
  - modellazione dell’ambiente simulativo
  - implementazione del comportamento degli automobilisti per quanto riguarda l’interazione con gli elementi dello scenario (strade e semafori).

- **Riccardo Bacca**: 
  - creazione dell’interfaccia utente (modifica parametri e selezione scenari), 
  - implementazione del comportamento degli automobilisti per quanto riguarda l’interazione con gli altri guidatori.

- **Riccardo Battistini**: 
  - produzione di grafici, 
  - gestione dei thread.

- **Stefano Camporesi**: 
  - modellazione dei veicoli e della strada
  - gestione dei semafori.

## EXTRA - Relazione in latex

- Installare un ambiente di compilazione in locale,
- impratichirsi alla scrittura in latex,
- trovare un modo per condividere facilmente il documento,
- assicurarsi che tutti abbiamo strumenti e documentazione per consultare e modificare la relazione,
- aggiornare la relazione mano a mano che il progetto va avanti.



