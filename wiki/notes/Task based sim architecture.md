---
title: Task based sim architecture
created: '2020-04-07T08:23:47.858Z'
modified: '2020-04-07T13:55:58.686Z'
---

# Task based sim architecture

A disposisizione un thread pool di pochi thread per eseguire i task.

Tipologie di task:
- time-fixed;
- ...

Alcuni possibili Task:

Riferiti alla strada
Riferiti alle entità
Riferiti allo stato del veicolo (entering, exiting, moving, idle)
Riferiti al semaforo (verde, giallo, rosso)

### Durante la simulazione (RUNNING), di tipo PERIODIC

**Model**

- handleEnteringVehicle
- handleExitingVehicle
- moveVehicle
- managePhase
- VehicleTracker
- TrafficLightTracker

**View**

- updateStats
- updateSimView
  - updateCarSprite
  - updateSemSprite
  - updateHUD

**Tabella delle corrispondenze**

| model                 | view                        |
|-----------------------|-----------------------------|
| handleEnteringVehicle | updateCarSprite             |
| handleExitingVehicle  | updateCarSprite             |
| moveVehicle           | updateCarSprite             |
| managePhase           | updateSemSprite, updateHUD  |
| VehicleTracker        | updateStats                 |
| TrafficLightTracker   | updateStats                 |


### Durante il setup della simulazione (WAITING), NON svolti dal threadpool (non serve concorrenza)

**Model**

- buildEnvironment;

**View**

- buildVoidStats;
- buildSimView;
  - buildBackground;
  - buildSemView;
  - buildHUD;

> **NB:** i task non sono omogeneei e indipendenti fra loro
> **NB:** task relativi all'aggiornamento dei componenti della GUI sono gestiti dall'EDT, task relativi alla trasmissione dell'input al model da parte del controller sono gestiti da task owned dal thread pool invece.
