---
title: Directory del progetto
created: '2020-02-28T10:06:29.697Z'
modified: '2020-03-24T11:25:31.656Z'
---

## Directory del progetto

[[Index|Index.md]]

---

```
.
├── bin
│   └── main
│       ├── application
│       ├── controller
│       ├── model
│       └── view
├── build.gradle.kts
├── config
├── doc
├── gradle
│   └── wrapper
│       ├── gradle-wrapper.jar
│       └── gradle-wrapper.properties
├── gradlew
├── gradlew.bat
├── LICENSE
├── pmd.xml
├── README.md
└── src
    ├── main
    │   ├── java
    │   └── resources
    └── test
        ├── java
        └── resources

```


- Il file **LICENSE** contiene la licenza che rende il software da noi prodotto open-source. Al momento si è scelta la licenza MIT. 

  Per ulteriori informazioni: [elenco delle principali licenze](https://help.github.com/en/github/creating-cloning-and-archiving-repositories/licensing-a-repository).

- Il **README.md** contiene una breve presentazione del progetto e spiega come avviare il programma. (formattata in Markdown, come questo file)

`traffic-sim` è la cartella principale del progetto e contiene le seguenti sottodirectory.

- **src**: contiene esclusivamente il codice sorgente dell’applicazione organizzato in package. Contiene i file .java che dovranno poi essere compilati per eseguire l'applicazione. È articolata nel seguente modo:

```
# SOGGETTA A CAMBIAMENTI
.
├── main
│   ├── java
│   └── resources
└── test
    ├── java
    └── resources

```

- **bin**: contiene:
  - la versione compilata del software,
  - una copia delle risorse,
  - viene interamente manutenuta dall’IDE, non è tracciata dalla repository (ognuno ha la sua copia locale e non esiste nella repository centrale).

---

- **doc** contiene:
  - la relazione in latex compilata come file pdf (documentazione non rigenerabile), 
  - la javadoc (documentazione rigenerabile),
  - il report di `cpd`, un'utility che può essere eseguita dall'estensione di Eclipse pmd per segnalare tutte le porzioni di codice duplicate nel progetto. (in linea di massima meno codice duplicato c'è meglio è)

- **res** contiene le risorse ovvero:
  - le **immagini** impiegate per la resa della grafica nel programma (elementi della strada, veicoli, semaforo, ecc.), 
  - i **file di testo** in cui si memorizzano le informazioni ricavate dalle simulazioni e salvate dall'utente, 
  - i **file di configurazione** che contengono gli scenari che creeremo.

- **test** è la cartella in cui si metteranno alcune classi sui quali effettuare test automatizzati con JUnit.

---

- **config** è la cartella che contiene i file di regole necessarie al funzionamento delle estensioni di Eclipse secondo le indicazioni dei professori.

**pmd.xml** è il file delle regole dell'estensione pmd. È presente una copia esterna alla cartella config che **va lasciata dov'è** perché il file possa essere letto senza problemi su ogni piattaforma. 

**checkstyle.xml, suppression.xml e style.xml** sono file delle regole dell'estensione checkstyle. Anche in questo caso il file style.xml si trova all'esterno e non dentro config e **va lasciato lì**.

**excludes.xml** è un file impiegato da spotbugs.
