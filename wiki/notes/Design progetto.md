---
title: Design progetto
created: '2020-03-09T10:59:04.261Z'
modified: '2020-03-14T08:54:48.630Z'
---

# Design progetto

**Patterns**:
- **architetturali**:
	- organizzazione progetto (model, View, controller)
- **progettuali**:
  - **creazionale**: 
    - creare istanze di oggetti
  - **strutturale**:
    - composizione degli oggetti
  - **comportamentale**:
    - interazione e distribuzione di responsabilità tra classi e oggetti

**Basi del progetto**:
	- basato sulle iterazioni di un cronometro gestito da un thread
		- ad ogni iterazione ogni macchina e ogni semaforo si aggiorna (ogni entità)

- **fondamenta di model e view**:
  - griglia basilare in 2 dimensioni
  - sopra la griglia inserito un grafo (pesato e orientato) rappresentante la strada e dotato dei seguenti attributi per ogni nodo:
    - *costo del movimento di ogni auto* (variabile anche a seconda di meteo, pendenza)
    - *verso di movimento*
    - possibile memorizzazione su lista linkata
    - consente una grande estensibilità per i diversi tipi di scenari implementati nella simulazione
  - La strada è quindi una struttura dati che inserisci nei nodi del grafo dai quali si passa
  - in seguito ogni nodo viene occupato o meno dalle macchine per identificare il movimento di queste ultime

- **classe semaforo**:
  - gestita da un thread
  - entità separata che agisce su ambiente in 2 dimensioni
  - prendendo spunto dalla slide di Ricci
  - abbiamo un set di regole per il semaforo, quindi algoritmi, ognuno dotato di implementazione
  in questo modo l’input viene inserito solo all’avvio del programma e abbiamo facile estensibilità per quanto riguarda il semaforo dinamico
  - interpretabile anche come una macchina in posizione fissa

- **classe veicolo**:
  - gestita da 2 thread per ciascuna delle 2 strade
  - entità separata che agisce su ambiente in 2 dimensioni
  - abbiamo un set di regole come per il semaforo che regolano aspetti come:
    - macchina avanza solo nella direzione consentita (grafo orientato decide verso, macchina vede orientamento)
    - macchina ferma se luce rossa, o macchine di fronte a lei (deposito in fondo tra auto, ultimo esame)
    - macchine gestiscono la distanza dalla macchina/non-macchina davanti (accelerazione, decelerazione, Cruise control)
  - diversi tipi di veicoli (auto, camion):
    - Interfaccia veicolo con caratteristiche comuni
    - Poi implementata da classi diverse

- utilizzo funzionalità LOG
- utilizzo funzionalità per gestione file nei quali saranno memorizzati i vari scenari, o meglio, i valori delle variabili globali relativi ai vari scenari

- thread totali:
  - Event dispatcher Thread (creato dall’interfaccia grafica all’avvio, se si blocca lui, si bloccano tutti):
    - thread per:
      - statistiche
      - 2 strade
      - cronometro (iterazioni)
=======
modified: '2020-03-07T09:38:29.290Z'
---

# Design progetto

**Patterns**:
- **architetturali**:
  - organizzazione progetto (model, View, controller)
- **progettuali**:
  - **creazionale**: 
    - creare istanze di oggetti
  - **strutturale**:
    - composizione degli oggetti
  - **comportamentale**:
    - interazione e distribuzione di responsabilità tra classi e oggetti

**Basi del progetto**:
	- basato sulle iterazioni di un cronometro gestito da un thread
		- ad ogni iterazione ogni macchina e ogni semaforo si aggiorna (ogni entità)

- **fondamenta di model e view**:
  - griglia basilare in 2 dimensioni
  - sopra la griglia inserito un grafo (pesato e orientato) rappresentante la strada e dotato dei seguenti attributi per ogni nodo:
    - *costo del movimento di ogni auto* (variabile anche a seconda di meteo, pendenza)
    - *verso di movimento*
    - possibile memorizzazione su lista linkata
    - consente una grande estensibilità per i diversi tipi di scenari implementati nella simulazione
  - La strada è quindi una struttura dati che inserisci nei nodi del grafo dai quali si passa
  - in seguito ogni nodo viene occupato o meno dalle macchine per identificare il movimento di queste ultime

- **classe semaforo**:
  - gestita da un thread
  - entità separata che agisce su ambiente in 2 dimensioni
  - prendendo spunto dalla slide di Ricci
  - abbiamo un set di regole per il semaforo, quindi algoritmi, ognuno dotato di implementazione
  in questo modo l’input viene inserito solo all’avvio del programma e abbiamo facile estensibilità per quanto riguarda il semaforo dinamico
  - interpretabile anche come una macchina in posizione fissa

- **classe veicolo**:
  - gestita da 2 thread per ciascuna delle 2 strade
  - entità separata che agisce su ambiente in 2 dimensioni
  - abbiamo un set di regole come per il semaforo che regolano aspetti come:
    - macchina avanza solo nella direzione consentita (grafo orientato decide verso, macchina vede orientamento)
    - macchina ferma se luce rossa, o macchine di fronte a lei (deposito in fondo tra auto, ultimo esame)
    - macchine gestiscono la distanza dalla macchina/non-macchina davanti (accelerazione, decelerazione, Cruise control)
  - diversi tipi di veicoli (auto, camion):
    - Interfaccia veicolo con caratteristiche comuni
    - Poi implementata da classi diverse

- utilizzo funzionalità LOG
- utilizzo funzionalità per gestione file nei quali saranno memorizzati i vari scenari, o meglio, i valori delle variabili globali relativi ai vari scenari

- thread totali:
  - Event dispatcher Thread (creato dall’interfaccia grafica all’avvio, se si blocca lui, si bloccano tutti):
    - thread per:
      - statistiche
      - 2 strade
      - cronometro (iterazioni)
>>>>>>> d4188813bfb981b4a48bebf8a50fea29f23d9048
      - semaforo
