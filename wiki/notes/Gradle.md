---
title: Gradle
created: '2020-02-27T11:20:35.735Z'
modified: '2020-03-24T11:25:31.542Z'
---

# Gradle

[[Index|Index.md]]

---

- Perché usiamo Gradle?

Un progetto Java se importato in Eclipse come “Gradle Project” scarica automaticamente i jar necessari e li mette nel classpath.

Usiamo perciò Gradle come **gestore di dipendenze** del progetto. 

(non tanto per build automation o software deployment e altri concetti avanzati di ingegneria del processo di produzione software che non abbiamo visto nel corso)

---

- Gradle CLI (interfaccia a linea di comando)

Si possono far eseguire dei task a Gradle da terminale (anche dall'interfaccia fornita da Eclipse) come la compilazione di file Java, **l'impacchettamento di un jar**, ecc.

Per maggiori informazioni basta digitare `gradle` sul terminale e seguire le istruzioni.

---

- Gradle richiede alcuni appositi **script**, messi a disposizione durante il corso, ed un **wrapper**, per poter essere eseguito.

Un elenco di file e directory impiegati da Gradle.

```
.
├── build.gradle.kts
├── gradle
│   └── wrapper
│       ├── gradle-wrapper.jar
│       └── gradle-wrapper.properties
├── gradlew
└── gradlew.bat
```

L'unico file da configurare è `build.gradle.kts`, uno script in kotlin:

<details>
<summary> build.gradle.kts </summary>

```
// Componenti per aggiungere configurazioni e task 
// Bisogna aggiungere il task per creare il file .jar
plugins {
    java
    application
    /*
     * Adds tasks to export a runnable jar.
     * In order to create it, launch the "shadowJar" task.
     * The runnable jar will be found in build/libs/projectname-all.jar
     */
    id("com.github.johnrengelman.shadow") version "5.2.0"
}

// Indica da dove scaricare i .jar delle librerie
repositories {
    jcenter()
}

// Indica quali moduli di javaFx prelevare
val javaFXModules = listOf(
    "base",
    "controls",
    "fxml",
    "swing",
    "graphics"
)

// Indica su quali piattaforme deve poter essere caricato il progetto
val supportedPlatforms = listOf("linux", "mac", "win")

// Indica quali librerie inserire nel progetto, nome della libreria, nome del file e versione
// separati da : e inclusi tra ("")
dependencies {

    compile("commons-io:commons-io:2.6")  // commons.io per gestione I/O
    compile("org.slf4j:slf4j-api:1.7.30") // slf4j per il logging
    implementation("com.google.guava:guava:28.1- jre")  // guava

    for (platform in supportedPlatforms) {  // javaFx
        for (module in javaFXModules) {
            implementation("org.openjfx:javafx-$module:13:$platform")
        }
    }
    
    // JUnit 5
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.5.2")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.5.2")
}

// Indica di usare JUnit per effettuare il task Test
tasks.withType<Test> {
    useJUnitPlatform()
}

// indica da dove parte l'applicazione (file Launcher.java in src/main/java/application)
application {
    mainClassName = "application.Launcher"
}

```
</details>

---

- Gradle richiede una particolare struttura del progetto.

Rispetto alla struttura di default bisogna rilocare le cartelle **src, res e test**. (notare anche i cambiamenti in **bin** e la scomparsa di **lib**)

**Senza Gradle**

```
.
├── bin
│   ├── application
│   ├── controller
│   ├── model
│   └── view
├── config
├── doc
├── lib
├── LICENSE
├── pmd.xml
├── README.md
├── res
├── src
│   ├── application
│   ├── controller
│   ├── model
│   └── view
└── test

```
**Con Gradle**

```
.
├── bin
│   └── main
│       ├── application
│       ├── controller
│       ├── model
│       └── view
├── build.gradle.kts
├── config
├── doc
├── gradle
│   └── wrapper
│       ├── gradle-wrapper.jar
│       └── gradle-wrapper.properties
├── gradlew
├── gradlew.bat
├── LICENSE
├── pmd.xml
├── README.md
└── src
    ├── main
    │   ├── java
    │   └── resources
    └── test
        ├── java
        └── resources

```

---

- Conclusioni

Una volta configurata la corretta struttura del progetto e inseriti gli script, per gestire le dipendenze non sarà necessario altro. Basterà importare la prima volta il progetto in  Eclipse come "Gradle Project".

