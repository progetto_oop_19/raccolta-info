---
title: Appunti vari
created: '2020-04-01T08:59:54.956Z'
modified: '2020-04-01T17:12:24.624Z'
---

# Appunti vari

Da Counter la classe Timer che determina un cambiamento di stato di Semaforo.
Questo cambiamento di stato determina una reazione diversa all'invio o alla ricezione di eventi.

Timer deve poter essere cambiato dall'utente controller-view.

Una mappa del tipo Map<Class, Instance> ??? @ClassToInstanceMap

@RangeMap può essere impiegata al posto di una EnumMap per `ModelLimits`

@Table per strutture del tip Map<String, Map< String, Integer>>, 

@Graph, magari si può esportare un grafo in una table per consultazione (immutableTable), lettura da parte della view dopo comuputazione.

Grafo -> Tavola<Position, Vehicle, Stats> -> raccolta singoli pacchetti di informazione per creare statistiche globali.

PositionCompare è una classe MOLTO utile per confrontare la posizione tra due oggetti. In teoria può essere usata per da uno stesso oggetto per confrontare la posizione con se stesso nel tempo.

Algoritmo shortest path per determinare il percorso e algoritmo di ottimizzazione della velocità rispetto al percorso

In `AbstractEntityView` metodi per aggiungere o rimuovere un `EntityView` dal `GameWorld`.

Per ciascuna entità che varia dinamicamente si crea una classe `<Nome>View`, quindi avremo CarView, 

Ogni `EntityView` ha una posizione e una dimensione. La posizione è indicata con il tipo `Point2D`. È implicito che ogni entity è rappresentata come un rettangolo (caso particolare quadrato) 

Limiti di velocità locali e ostacoli oltre al meteo

Strada con un senso con parcheggi, tombino che diventa ostacolo con la pioggia, precedenze e relativa segnaletica

Per creare i percorsi si impiegano degli `Iterator<Point2D>` che creano dei path/percorsi

Per le statistiche si crea un oggetto di tipo `EntityStatistics` di nome "tracker".

Le classi di utility vanno `final`!
