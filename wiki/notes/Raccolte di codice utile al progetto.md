---
title: Raccolte di codice utile al progetto
created: '2020-02-26T08:36:37.160Z'
modified: '2020-03-24T11:25:31.228Z'
---

# Raccolte di codice utile al progetto

File UseGetResource.java

~~~java

/**
 * Example class loading an image with {@link Class#getResource(String)}.
 *
 */
public final class UseGetResource {

    private UseGetResource() {
    }

    public static void main(final String... args) throws IOException {
        /*
         * Access resources as streams using InputStream and BufferedReader
         */
        final InputStream in = ClassLoader.getSystemResourceAsStream("settings/settings");
        String line = "Error accessing the resource";
        try (BufferedReader br = new BufferedReader(new InputStreamReader(in))) {
            line = br.readLine();
        }
        final JLabel lab2 = new JLabel(line);
        /*
         * Loading of icons and images is made very easy!
         */
        final URL imgURL = ClassLoader.getSystemResource("images/gandalf.jpg");
        final ImageIcon icon = new ImageIcon(imgURL);
        /*
         * From now on, it's just plain GUI construction
         */
        final JLabel lab1 = new JLabel(icon);
        final JPanel pan = new JPanel();
        pan.setLayout(new BoxLayout(pan, BoxLayout.Y_AXIS));
        pan.add(lab1);
        pan.add(lab2);
        final JFrame f = new JFrame("Image loader");
        f.getContentPane().add(pan);
        f.setResizable(false);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.pack();

        // emula il comportamento di una finestra nativa rispetto alla piattaforma in cui è lanciata
        f.setLocationByPlatform(true);  
        
        f.setVisible(true);
    }

}
~~~
