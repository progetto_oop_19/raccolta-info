---
title: Basi di partenza
created: '2020-02-29T09:00:43.416Z'
modified: '2020-03-24T11:24:23.895Z'
---

# Basi di partenza

**È molto più importante realizzare un'applicazione di qualità che concentrarsi sulla quantità di funzionalità messe a disposizione dal programma**.

**I pattern andrebbero usati solo quando c'è un problema da risolvere, altrimenti diventa difficile giustificarne anche la scelta. È inutile cercare di inserire quanti più pattern possibile**.

---

## Ambiente

- L'ambiente (environment) è rappresentato da una griglia di celle che può essere creata con una factory di griglie a partire dal tipo cella.

*Dal codice del Corso:* Esami 2019 -> a01b.sol1 

#### Strade

- Le strade sono rappresentate da grafi pesati e orientati che possono essere creati con una factory di grafi a partire dal tipo cella (ogni nodo del grafo è rappresentato da una distinta cella)

*Dal codice del Corso:* Esami 2019 -> a01a.sol1

- Orientato perché indica il senso di marcia (che sarà sempre rispettato)

- Pesato perché permette di definire ostacoli locali (strada sconnessa, lavori stradali) o condizioni che interessano le strade di tutto lo scenario (pioggia, pendenza). Inoltre si integra bene con il meccanismo di velocità variabili (punti movimento che vanno spessi per attraversare ciascun collegamento tra nodi del grafo (archi)).

> **Nota:** ostacoli, condizioni meteo e pendenza fanno parte delle caratteristiche opzionali. Scrivere codice che permetta di aggiungere nuovi condizioni definite a livello globale o locale nello scenario senza troppe modifiche o ristrutturazioni è indice di qualità. Questo è molto più importante rispetto al riuscire a implementare o meno queste condizioni globali o locali.

---

## Entità
- Ci sono due entità nello scenario (caratteristiche comuni dovute al muoversi in uno spazio bidimensionale), **veicoli** e **semafori**.

- Si potrebbe impiegare un builder con interfaccia fluida e controllo d'errore per ciascuna entità.

*Dal codice del Corso:* Esami 2018 -> a02a.sol1 

#### Semafori

(in teoria se implementiamo i semafori dinamici si potrebbe pensare di creare una classe astratta che raggruppi le caratteristiche comuni a tutti i semafori e poi di impiegare classi specifiche per definire una semaforo statico o dinamico)

(alternativamente dato che la distinzione riguarda solo statico o dinamico non serve creare tutta questa struttura che comunque garantirebbe un'estendibilità che in questo caso è eccessiva, perciò si potrebbe configurare a livello di builder se creare un semaforo statico o dinamico)

-> si creano tipologie diverse di semafori senza il concetto di set di regole (meno elegante)

 > **Nota:** i semafori dinamici fanno parte delle funzionalità opzionali. Scrivere codice che permetta di aggiungere nuovi tipi di semafori senza troppe modifiche o ristrutturazioni è indice di qualità. Questo è molto più importante rispetto al riuscire a implementare o meno il semaforo dinamico.


#### Veicoli

- Per garantire maggiore estendibilità si può creare una classa astratta veicolo e un builder astratto per il tipo veicolo. In seguito si può implementare una sottoclasse macchina e il relativo builder. 

  Si può ripetere il procedimento per camion, moto, ambulanze, ecc. In questo modo sfruttiamo la chiarezza e semplicità a livello di API del builder e garantiamo estendibilità (data dall'impiego di classi astratte).

> **Nota:** la creazione di tipologie di veicoli diverse da una macchina fa parte delle funzionalità opzionali. Scrivere codice che permetta di aggiungere nuovi tipi di veicoli senza troppe modifiche o ristrutturazioni è indice di qualità. Questo è molto più importante rispetto al riuscire a implementare o meno altre tipologie di veicoli.

---

## Comportamento dei semafori

Comprende fasi semaforiche di lunghezza variabile in base all'input dell'utente o di lunghezza automatica (statica o dinamica).

Un semaforo dinamico richiede un set di regole perché il suo comportamento sia definito. Ciò perché il suo comportamento varia in base all'evoluzione dello scenario.

-> si crea un unico semaforo e si carica un set di regole diverso per alterarne il funzionamento

---

## Comportamento dei veicoli

Definito complessivamente da quattro tipi di interazioni:

- Interazione tra veicoli
- Interazione tra veicolo e scenario

#### Interazione tra veicoli

- Il movimento dei veicoli è codificato da un insieme di regole di aggiornamento. Queste regole definiscono l'interazione tra veicoli.

  Ogni tipologia di veicolo può modificare il suo comportamento in base a queste regole durante l'esecuzione della simulazione (a partire dalle impostazione scelte dall'utente, diversi parametri ai quali sono applicate le regole).

  Il set di regole non non dovrebbe essere esteso (almeno relativamente agli scenari che creeremo, in teoria c'è l'insieme di regole per il cambio di corsia, anche se non le implementiamo si può costruire il programma in modo che sia semplice in un secondo momento aggiungere altre regole da far rispettare ai veicoli)
  (inoltre dovrebbe essere semplice anche modificare il set di regole iniziale)

  In altri termini *il funzionamento dei veicoli dovrebbe dipendere da un set di regole caricato al momento della creazione*.
  
> **Nota:** l'aggiunta di nuovi set di regole (per il cambio di corsia ad esempio) non è nemmeno parte delle funzionalità opzionali. Tuttavia garantire la possibilità di definire nuove regole e caricarle con semplicità, senza troppe modifiche o ristrutturazioni del codice è indice di qualità. (In generale si semplifica il lavoro di altri programmatori sul software che sviluppiamo)

---

#### Interazione tra veicolo e scenario 

Comprende interazione veicolo-semaforo, veicolo-strada, veicolo-configurazione scenario (locale o globale)

