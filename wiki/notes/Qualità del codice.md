---
title: Qualità del codice
created: '2020-02-28T09:59:57.301Z'
modified: '2020-03-24T11:25:31.330Z'
---

# Qualità del codice

[[Index|Index.md]]

---

**Tavola dei contenuti**

1. Code smell e refactoring
2. Code checking

---

## 1. Code smell e refactoring

Con il termine **code smell** si intendono le debolezze di progettazione che riducono la qualità del software (non si intendono i bug). Indica un insieme di caratteristiche del codice sorgente.

**NB:** il code smell non è definito in termini assoluti, comprende sempre il giudizion soggettivo del programmatore che analizza il codice.

Un **bug** è piuttosto un errore nel codice sorgente che provoca errori in un'applicazione. Più generalmente può indicare un difetto di progettazione che provoca un comportamento imprevisto.

Il **refactoring** consiste in un insieme di azioni di ristrutturazione del codice volte a migliorarne la struttura abbassandone la complessità e lasciando inalterate le funzionalità.

~~> Refactoring is a disciplined technique for restructuring an existing body of~~
~~code, altering its internal structure without changing its external behavior.~~
~~— refactoring.com~~

~~Il **refactoring** è una tecnica che consente di ristrutturare una porzione di codice preesistente alterando la sua struttura interna senza modificare il comportamento esterno, ovvero senza alterare il suo funzionamento rispetto agli altri blocchi di codice con cui interagisce.~~

---

## 2. Code checking
 
Checkstyle, pmd e spotbugs sono code checkers che possono essere eseguiti in due modalità:

- **Stand-alone**: il software viene eseguito e genera un report, in particolare i seguenti file:
  - **cpd-report.txt** per il codice duplicato,
  - **pmd-report.txt** e **style.txt** per warning riguardo la qualità del codice.

- Come **plug-in**: il software viene integrato con l'IDE e segnala i problemi in forma di warning.

