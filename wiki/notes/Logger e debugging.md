---
title: Logger e debugging
created: '2020-02-24T16:15:15.243Z'
modified: '2020-03-24T11:25:31.479Z'
---

# Logger e debugging

[[Index|Index.md]]

---

- Perché si impiega un logger?

Perché si tratta di uno di quei casi in cui è particolarmente utile impiegarlo e i vantaggi superano gli svantaggi. In particolare offre la possibilità di fare debugging del codice in modo simile a quanto fatto con Ghini in SO.

(Sconsigliato l'uso di meccanismi di logging in diapositiva 38 nella dispensa 03-eclipse.pdf)

> Threading/concurrency problems are notoriously difficult to replicate - which is one of the reasons why you should design to avoid or at least minimize the probabilities. [...]

> With these, the path I usually take, besides **extensive testing to replicate**, is to reason about the possibilities, and **try to log information to prove or disprove theories**. [...] 

> Make use of tools like **JVisualVM** and remote connect profilers - they can be a life saver if you can connect to a system in an error state and inspect the threads and objects. 

> One last thing, try to use concurrency objects distributed with the system libraries - e.g in Java `java.util.concurrent` is your friend. Writing your own concurrency control objects is hard and fraught with danger; **leave it to the experts**, if you have a choice.


da stackoverflow
[[https://stackoverflow.com/questions/499634/how-to-detect-and-debug-multi-threading-problems]]

---

- Come impiegare un logger?

Studia documentazione di `java.util.logging`.

da stackoverflow:
[[https://stackoverflow.com/questions/5950557/good-examples-using-java-util-logging#16448421]]

---

- In cosa consiste la modalità di debug?

Ad ogni esecuzione del programma viene creato un file di log che permette di osservare l'andamento dell'applicazione, se si attiva la modalità di debug l'output del logger viene ridirezionato in modo da scrivere il file di log **e** dare informazioni in tempo reale sullo stdout (area di testo apposita nell'applicazione o in un'altra finestra).

- A cosa serve il logger?

Permette di tracciare lo stato interno del programma in fase di esecuzione: valore corrente di variabili, campi, parametri, ecc.

- Quando si deve registrare un'informazione nel logger?

Quando può essere utile per il debugging del programma come sistema concorrente.
