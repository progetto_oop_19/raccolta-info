---
title: Configurazione locale di Eclipse
created: '2020-02-25T15:26:11.963Z'
modified: '2020-03-24T11:25:31.747Z'
---

# Configurazione locale di Eclipse

[[Index|Index.md]]

---

*Tratto dalle dispense di laboratorio 8*

Si vuole creare un ambiente di sviluppo portabile.

> *Write once, run everywhere* 
-Sun Microsystems

---

Perché sfruttare la configurazione locale piuttosto che quella globale?

La configurazione globale viene applicata a tutti i progetti del workspace a meno che non sia sovrascritta localmente.

- **Veloce da configurare**: si configura una volta, e i settaggi sono applicati su tutti i progetti
- **Poco portabile**: se inviamo il progetto ad un collaboratore, questi settaggi non saranno inviati: c’è il rischio di inconsistenze
- Ottimo da usare quando si hanno molti progetti nel workspace con la stessa configurazione, e **si sviluppa da soli**.

La configurazione locale va messa a punto singolarmente per ciascun progetto. Bisogna verificare che quella globale non la stia sovrascrivendo.

- **Alta flessibilità**: si possono specificare regole diverse per progetti diversi
- **Alta portabilità**: se si invia il progetto ad un collaboratore, queste impostazioni vengono mantenute
- Ottimo da usare quando si hanno pochi progetti, o ai progetti serve una configurazione diversa, oppure quando **si lavora in modo collaborativo**.

---

## Configurazione di encoding e newline in Eclipse

- Selezione del progetto → Click destro → Preferences → Resource
- Text file encoding → Other → UTF-8
- New text file line delimiter → Other → Unix
- Apply → OK

## Configurazione del formattatore di Eclipse

- Window → Preferences → Java → Code Style → Formatter
- New -> nome: Java Conventions OOP, base: java conventions [built in]
- Change *Indentation*
  - Tab policy to **spaces only**
  - Indentation size to **4**
- Change *Line Wrapping*
  - Maximum line width to **120**

## Configurazioni dei plugin di Eclipse

*Vedere diapositive 54-65 Lab08*

## Scorciatoie da tastiera di Eclipse

CTRL+1: quick-fix contestuale per errori (molto potente)
ALT+SHIFT+R: refactoring di campi/metodi/classi
CTRL+SHIFT+/: commento delle linee selezionate
CTRL+PGUP/PGDOWN: per muoversi di uno step avanti (PGUP) o
indietro (PGDOWN) tra la lista di sorgenti correntemente aperti
F3 (o CTRL+CLICK): ci sposta alla definizione di un dato elemento
CTRL+SHIFT+L: lista delle shortcut disponibili per il dato contesto
CTRL+SHIFT+O: include in automatico tutti gli import necessari
sulla base delle classi utilizzate nel sorgente corrente
CTRL + .: sposta il cursorse al successivo errore/warning
CTRL+F8: consente di spostarsi tra le varie perspective
CTRL+J: search incrementale, senza l’uso di GUI
ALT+SHIFT+S: da l’accesso a un insieme di wizard con cui
automatizzare la scrittura di costruttori, getter, setter, etc.


