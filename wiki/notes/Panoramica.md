---
title: Panoramica
created: '2020-02-24T15:17:53.766Z'
modified: '2020-03-24T11:25:31.371Z'
---

# Panoramica 

[[Index|Index.md]]

---

## Introduzione

Vogliamo creare un modello che simuli un sistema di traffico di dimensioni ridotte.
(Rappresenta un esempio concreto di simulazione in 2D di Automa Cellulare, si tratta di modellare
il comportamento umano in uno spazio finito chiuso)

In particolare vogliamo creare un modello che descriva il comportamento come risultante dall'*interazione reciproca di entità discrete* (modello microscopico).

Il principale limite di questo tipo di modello sta nel considerare il comportamento di ogni veicolo come uniforme. (non si riesce a modellare il comportamento di ciascun automobilista, si applicano regole uniformi a tutte le macchine). 

## Un possibile modello

Il modello di traffico da definire è una **griglia di LxL celle** con *bordi chiusi* (non si sconfina nel lato opposto).

Il numero totale di veicoli N nel sistema è costante ed è definito da appositi parametri *memorizzati in appositi file*.

~~~Java
// Si specifica il numero di veicoli per ciascuna strada
CARS-NORTHBOUND = 10;
CARS-EASTBOUND = 10;
// Si ottiene N
CARS-OVERALL = CARS-NORTHBOUND + CARS-EASTBOUND;
~~~

L'utente può controllare:
  - la **frequenza delle macchine** che provengono da ogni direzione;
  - la **velocità** delle macchine;
  - la **tempistica del semaforo** all'intersezione.

Dopo la *fase di configurazione*, in cui l'utente determina frequenza e velocità delle macchine, l'utente dovrebbe avviare la simulazione e adattare la tempistica del semaforo per **minimizzare il tempo di attesa** delle macchine che viaggiano lungo l'intersezione.

## Come funziona

Alcune regole comuni a tutti gli scenari:

- Ogni cella può essere occupata al massimo da un veicolo oppure essere vuota. (niente scontri) 
- Ad ogni cella corrisponde un segmento di strada di lunghezza $l$. 
- La **densità del traffico globale** (a livello dell'intero scenario) è data dalla seguente formula: $$\mu=\frac{N}{L}$$ si possono poi definire le densità per ciascuna strada definendo $x$ parametri $N_i$ dove $x$ indica il numero di strade e $N_i$ il numero di macchine dell'i-esima strada.

- Ogni macchina ha una velocità che oscilla tra 0 e `VEL-MAX`. (definita nel *pannello di configurazione*) 
- La velocità corrisponde al numero di celle di cui avanza un'auto in un'iterazione. (solo celle valide) 
- Il **movimento dei veicoli** attraverso le celle è definito da un insieme di **regole di aggiornamento**. 

<details>
<summary> Regole di aggiornamento </summary>

Per questo modello si adotta il seguente insieme di regole per il movimento delle macchine:

### Informale (con semaforo e quindi con cambi di velocità estremi, ovvero richiesta di frenata)

1. Una macchina *avanza* solo nella direzione che gli è stata assegnata inizialmente o fermarsi, in altri termini **le macchine non cambiano mai direzione.**

2. Una macchina *si ferma* **se ci sono macchine di fronte a lei e/o se la luce è rossa**.

3. Una macchina che si muove troppo velocemente, che presto dovrà fermarsi, rallenta in modo proporzionale allo spazio libero a disposizione (distanza fino al primo spazio non libero) fino ad un massimo di `MAX-BRAKE`.

4. Una macchina che si muove troppo lentamente, che potrebbe accelerare, accelera in modo proporzionale allo spazio libero a disposizione fino a `MAX-ACC`.

5. ~~Se una macchina si trova nello stesso spazio di un'altra macchina si ha uno scontro ed entrambe le macchine vengono immediatamente rimosse dallo scenario.~~ (se non implementiamo gli scontri è una regola inutile)

### Formale (senza semaforo, da riformulare)

1) **Accelerazione** di un *veicolo libero* (non ostacolato da altri automobilisti): $$v(i+1)=v(i)+1$$
**NB:** La relazione è valida solo se $v(i)<v_{max}$ e $g(i)>v(i)+1$. (può essere visto come il *delta positivo* della velocità)

2) **Rallentamento** dovuto ad altri veicoli:  $$v(i)=g(i)$$
**NB:** La relazione è valida solo se $v(i)>g(i)-1$. (può essere visto come il *delta negativo* della velocità)

3) **Avanzamento** del veicolo: il veicolo si sposta di un numero di celle pari alla sua velocità $v(i)$.(rappresenta il *delta di spostamento*)

---

- Queste regole sono applicate **in parallelo, a ciascun veicolo e ad ogni iterazione**.
- La **durata di un'iterazione** può essere scelta per riflettere il livello di dettaglio nella simulazione desiderato.
- Si può scegliere perciò un'iterazione di lunghezza sufficientemente piccola per approssimare un sistema temporale continuo. (sistema in cui il tempo è entità continua e non discreta, in altri termini se un iterazione avviene ogni secondo piuttosto che ogni 10 si riesce a creare l'illusione di macchine in continuo movimento molto meglio) 
- Lo **stato del sistema ad una data iterazione** è determinato dalla distribuzione dei veicoli tra le celle e dalla velocità di ciascun veicolo in ogni cella. (per *debug*, *statistiche* e *salvataggio su file*)
  
- Si impiega la seguente **notazione**:

  - $p(i)$: posizione dell'i-esimo veicolo (*Pair* di coordinate cartesiane)
  - $v(i)$: velocità dell'i-esimo veicolo (intero compreso tra 0 e `VEL-MAX`)
  - $g(i)$: distanza tra l'i-esimo veicolo e il successivo $(i+1)$, data da: $g(i)=p(i+1)-p(i)$ (intero)

</details>

## Varianza della velocità e flusso massimo del traffico

La **varianza della velocità** aumenta notevolmente mano a mano che il sistema si avvicina al raggiungimento del **flusso massimo**, che costituisce l'obiettivo della maggior parte dei piani di traffico.

Un'**elevata varianza della velocità** indica che distinti veicoli nel sistema hanno velocità che variano notevolmente. Ciò significa inoltre che ciascun veicolo in ciascun viaggio nel sistema è soggetto a **frequenti cambi di velocità**.

Di conseguenza si ha una **maggiore variabilità nei tempi di viaggio**, ovvero nei tempi di attraversamento del sistema. Questo aumenta le probabilità di incidenti (che comunque non consideriamo per ora).

Perciò, per quanto possa sembrare controintuitivo, sarebbe meglio fare in modo che il traffico non raggiunga la massima portata del sistema, o in altri termini, che raggiunga il flusso massimo.
