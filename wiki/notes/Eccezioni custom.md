---
tags: [Relazione]
title: Eccezioni custom
created: '2020-04-16T09:35:15.418Z'
modified: '2020-04-16T10:26:04.333Z'
---

# Eccezioni custom

InvalidParameterException
SimulationAlreadyStartedException

The main reasons for introducing custom exceptions are:

- Business logic exceptions – Exceptions that are specific to the business logic and workflow. These help the application users or the developers understand what the exact problem is
- To catch and provide specific treatment to a subset of existing Java exceptions


