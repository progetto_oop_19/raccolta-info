---
title: Map to World to Point2D to Vector
created: '2020-04-02T07:43:17.112Z'
modified: '2020-04-05T08:04:31.889Z'
---

# Map to World to Point2D to Vector

In class `LoadLevelImpl`, where the tilemap is loaded:

~~~java
private Pair<Point2D, Dimension2D> mapPositionToWorld(final Map map,
            final double x, final double y,
            final double width, final double height) {
        final Dimension2D dim = new Dimension2D(width / map.getTileWidth(),
                height / map.getTileHeight());
        final Point2D pos = new Point2D(x / map.getTileWidth() + dim.getWidth() / 2,
                -(y / map.getTileHeight() + dim.getHeight() / 2));

        return new Pair<>(pos, dim);
    }
~~~

Alternativamente si può creare una griglia con Altezza e Larghezza specificate e popolarla di oggetti di tipo Strada, Semaforo o Sfondo ricorrendo alla classe di tipo view di ciascuno di questi oggetti che carica da un classe di utilità la texture/sprite associata.

In base alle specifiche fornite nel file YAML l'oggetto di tipo Strada potrà essere di tre tipi: limite, vuoto, riga centrale.

Il semaforo potra assumere una fase diversa

Lo sfondo rimarrà sostanzialmente lo stesso e sarà tracciato per ultimo.

Si ottiene così una griglia in 2D sulla quale si possono inserire le macchine.

`LoadTile` e `TileLayer`, LoadTile riceve un TileLayer che piazza le tessere sul Canvas. Si potrebbe fare con una factory?

### Package Utilities

**Gestione orientata agli eventi** che sfrutta componenti come Event, CompositeEvents, EventHandler ed EventSource (pattern Observer)

**Gestione dello spazio tramite vettori** per indicare sia intensità che direzione del moto

Point2D & Pair esistono già, perché creare Cartesian e Point2D ? Bastava creare la classe vettore. Tuttavia si può scegliere se fare una semplice implementazione in-house oppure se optare per il ricorso alle librerie.

In ogni caso la gestione tramite classi di utilità con campi final (per bloccare ereditarietà) per gestire le operazioni tra vettori tramite l'applicazione di altri vettori è molto comoda.
Modificatore static per metodi e campi, costruttore di default.

Per i vettori si potrebbe usare `org.apache.commons.math3`
Modelizzazione matematica su spazio euclideo/cartesiano ??
Entità in movimento e in grado di girare grazie all'applicazione di vettori.

### Package Loader

`YamlLoader` disponibile e `TmxLoader` anche, oltre al loader JSON.
Idealmente si crea lo scenario tramite Yaml, mentre i salvataggi si mantengono in JSON.
Alternativamente si possono gestire alcune impostazioni nel Yaml, caricare uno scenario predefinito in TmxLoader e gestire i salvataggi con JSON.

`ScenarioData` è la classe che viene mappata dal file Yaml. (configurazione)
`ScenarioSession` è la classe mappata dal file JSON (configurazione + stato)

Concetti di `ScenarioLoader`, interfaccia per eseguire gli `ScenarioBuilder` che presi i parametri da `ScenarioData`

`Scenario` rappresenta uno scenario che contiene tutte le entità, create tramite `EntityFactory`

Implementazione `ScenarioWithWeatherDecorator` ? Si parte da `ScenarioDecorator` e `BaseScenario`, con interfaccia implementata da `ScenarioDecorator` di nome `Scenario`

### Package entities

AbstractVehicleDecorator e CarVehicleDecorator, CamionVehicleDecorator.
Perché usare il Decorator piuttosto che impiegare una classe astratta (Template Method)?
 Fare in modo che un'entity acquisisca delle capacità estendendo una certa classe, anche iscrivendosi ad un observable.

 I manager sono gli observable.

 Concetti di start queue e destroy queue.
 ~~~java
 private final Queue<GameObject> startQueue;
 ~~~

### Package Controller

Si mette in ascolto degli eventi che accadono nel Model e notifica la View.
La view si mette in ascolto degli eventi del Controller *e quando riceve input notifica il Controller*.

---

La simulazione nel Model non va avanti per conto suo, mette a disposizione dei metodi per fornire informazioni sul suo stato e per farla proseguire. **Perciò attende che il Controller le dica che può proseguire.**

La simulazione potrebbe andare per conto suo e il controller periodicamente preleva informazioni **su richiesta della view** e le passa alla view.

---

TraceSaver e Tracefile in relazione al logging

---

The graph can be updated dynamically while the simulation is running.

The idea is that the interface to software `components' (or `beans') is restricted to a stream of user defined events. These components can be wired together on the y by calling an addEventListener() method. This is ideal for processing simulation results, where there is a trace 'stream' which can be tapped into and displayed. The simdiag package provides two types of bean, those which listen to a stream of events during a simula- tion (TraceEventListeners), and those which plot graphs (GraphEventListeners).

the simulation producing a stream of events which are displayed in a timing diagram and then forwarded to a `TraceSaver' for storage to a file.

The timing diagram bean ( gure 5) shows how the state of each entity changes over time.

---

DecisionMaker 


