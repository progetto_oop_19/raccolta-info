---
tags: [Relazione]
title: Tavola dei contenuti
created: '2020-03-01T09:26:45.513Z'
modified: '2020-03-24T11:25:40.871Z'
---

# Tavola dei contenuti

1. Analisi
  - Requisiti
  - Analisi e modello del dominio 

2. Design
  - Architettura
  - Design dettagliato

3. Sviluppo
  - Testing automatizzato
  - Metodologia di lavoro
  - Note di sviluppo

4. Commenti finali
  - Autovalutazione e lavori futuri
  - Difficoltà incontrate e commenti per i docenti
  
A Guida utente

