---
title: Qualità del software
created: '2020-02-25T15:27:37.342Z'
modified: '2020-03-24T11:25:31.287Z'
---

# Qualità del software

[[Index|Index.md]]

---

**Tavola dei contenuti**

1. Internazionalizzazione
2. Portabilità
3. GUI scalabili

## 1. Internazionalizzazione

La UI (User interface, Interfaccia utente) andrebbe definita una sola volta e il testo dovrebbe cambiare a seconda dell'impostazione della lingua di sistema (o di un'impostazione definita nell'applicazione).

(In realtà, non solo per la lingua ma anche per il formato dei numeri, la
valuta, le convenzioni sulla data, ecc.)

Java fornisce una architettura per l’internazionalizzazione, che fa uso di “ResourceBundle” e di una serie di file di supporto (properties files).

Per l'implementazione del supporto multilingua si consulti:
- [https://docs.oracle.com/javase/tutorial/i18n/resbundle/index.html](https://docs.oracle.com/javase/tutorial/i18n/resbundle/index.html)
- [http://tutorials.jenkov.com/java-internationalization/resourcebundle.html](http://tutorials.jenkov.com/java-internationalization/resourcebundle.html)

---

## 2. Portabilità

In `java.System`: `String getProperty(String p)`

- **Proprietà relative al file system**
  - `file.separator` — Restituisce \ per Windows e / per Unix
  - `java.home` — La directory di installazione di Java user.dir — La directory da cui il comando java è stato invocato
  - `user.home` — Restituisce la home directory dell’utente che ha lanciato java
  - `user.name` — Restituisce il nome utente

- **Proprietà relative al sistema**
  - `java.version` — La versione di java in uso. Si potrebbe decidere di non usare una funzionalità che si sa non esistere o essere buggata.
  - `os.arch` — L’architettura della CPU come rilevata dall’OS (es. x86, i386, amd64, x86 64, IA64N, arm, ecc.)
  - `os.name` — Il nome del sistema operativo (es. Linux, MacOS X, MacOS, Windows 8.1, Windows 10, Solaris, FreeBSD, ecc.). Ci potrebbero essere librerie non licenziate o non disponibili per alcune piattaforme.
  - `os.version` — Restituisce in Windows il numero di versione effettivo (es. Windows 10 restituisce 10.0), per MacOS il numero di versione (es. 10.3.4), per Linux la versione del kernel (es. 4.17)
 
---

## 3. GUI scalabili

### Buone pratiche

- **La dimensione di default della finestra va calcolata in base alla dimensione dello schermo.**
- **È opportuno non specificare dimensioni assolute in pixel per i componenti della GUI, ma dimensionarli in termini relativi rispetto al container.**
- **Anche per i layout è opportuno non utilizzare dimensioni fisse in pixel.**
- **L’utente deve essere libero di ridimensionare l’interfaccia a piacimento per adattarla alla propria configurazione di schermi.**
- I font possono essere allegati all’applicazione.
- La dimensione dei font può essere resa relativa alla dimensione corrente della view.

Questo perché bisogna garantire:

- **Flessibilità**
  - Diversamente dagli anni 90, i dispositivi oggi hanno una densità di pixel per area estremamente variabile. Si va da 120 PPI a 640 PPI, su schermi di dimensione estremamente variabile (da 3 a 200 pollici).

- **Supporto multipiattaforma**
  - Piattaforme diverse, anche a parità di schermo, possono adottare diverse convenzioni:
    - Diversa grandezza di bordi
    - Diversa spaziatura, dimensione e tipo di font
    - Diverso sistema di decorazioni
  - Questi elementi sono stabiliti dal compositor e non dallo sviluppatore dell’applicazione. Come indicazione generale vale che **un’applicazione ben sviluppata eredita il “look and feel” dal sistema su cui sta girando**.

