---
title: Forum
created: '2020-03-09T10:59:04.233Z'
modified: '2020-04-19T12:38:08.655Z'
---


B - A traffic simulation - Bacca Riccardo, Battistini Riccardo, Camporesi Stefano, Lazar Adrian - TrafficSimulator
Il gruppo si pone come obiettivo quello di realizzare un'applicazione per analizzare il flusso del traffico in uno scenario semplificato.
Applicando modelli matematici ai sistemi di trasporto è possibile realizzare del software per implementare e pianificare schemi di traffico su tratti urbani.
Il campo della simulazione dei trasporti è nato circa quarant'anni fa e ad oggi costituisce un'importante branca dell'ingegneria del traffico e della pianificazione dei trasporti.
Il software realizzato in tale ambito contribuisce notevolmente alla gestione delle reti di trasporto ed è impiegato da agenzie del settore a livello locale e nazionale.
Il progetto permette all’utente di capire, modificando alcuni parametri (numero, frequenza e velocità delle macchine) e la tempistica dei semafori, quali sono le combinazioni che ottimizzano il traffico (ovvero che minimizzano il tempo di attesa delle macchine che attraversano lo scenario).

Funzionalità minime ritenute obbligatorie:

    • Modellare il comportamento degli automobilisti secondo il car-following model, ovvero l'automobilista regola la distanza in base al veicolo che lo precede;
    • Implementazione scenario "intersezione" con relativa GUI (strada ad una corsia) con semafori;
    • Programmazione multi-thread per gestire gruppi di macchine;
    • Possibilità di modificare i parametri sperimentali (numero di veicoli in entrata, velocità media dei veicoli).
    - Creazione della classe BasicScenario
    - Creazione dello scenario Builder
    - Rendering della GUI 

Funzionalità opzionali:

    • Implementazione scenario “intersezione” con strade a due corsie e semafori;
    • ~~Realizzazione di semafori con comportamento dinamico in base al numero di macchine nello scenario con relativo pulsante di attivazione nella GUI;~~
    • Possibilità di variare la velocità in base alle condizioni meteo.

Challenge principali:

    • La realizzazione di grafici richiederà l'uso di librerie da identificare;
    • Implementare modelli matematici del traffico semplificati che definiscono il comportamento degli automobilisti e dei semafori.
    • L’interfaccia grafica non è di banale realizzazione e richiederà l'impiego di librerie da identificare.

Suddivisione del lavoro:
- Bacca: creazione dell’interfaccia utente (modifica parametri e selezione scenari), implementazione del comportamento degli automobilisti per quanto riguarda l’interazione con gli altri guidatori.
- Battistini: produzione di grafici, gestione dei thread.
- Camporesi: modellazione dei veicoli, della strada e gestione dei semafori.
- Lazar: modellazione dell’ambiente simulativo, implementazione del comportamento degli automobilisti per quanto riguarda l’interazione con gli elementi dello scenario (strade e semafori).

