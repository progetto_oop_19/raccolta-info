---
pinned: true
title: Algoritmi per funzionamento delle macchine
created: '2020-03-22T15:07:38.657Z'
modified: '2020-03-23T14:13:49.778Z'
---

# Algoritmi per funzionamento delle macchine

### Concetti introdotti per simulare un comportamento realistico

- **Area di controllo**: un  automobilista può alterare il suo comportamento solo in base alla macchina immediatamente successiva che si trova ad una distanza pari o inferiore alla sua area di controllo. In condizioni normali il campo di visione dovrebbe coincidere con la velocità massima. (forse anche poco di più) 

  L'area di controllo determina la capacità dell'automobilista di reagire all'alterazione dell'ambiente circostante (di fatto o il succedersi delle fasi semaforiche o la diversa velocità della macchina che gli sta davanti, unica manifestazione del comportamento di un automobilista)

- **Velocità**: indica di quante caselle si muove una macchina ad ogni iterazione. Le macchine NON possono scontrarsi nè sono possibili sorpassi.

- **Accelerazione**: indica la capacità di una macchina di alterare la sua velocità. 

  Ad esempio se una macchina ha un valore si accelerazione pari a 2 allora potrà:
  - accelerare di 1 o 2 unità,
  - decelerare di 1 o 2 unità,
  - non accelerare (acc. 0)

> **Nota:** per meglio comprendere le grandezze definite si possono impiegare le seguenti unità di misura:
> - **velocità: unità/tick**
> - **accelerazione: unità/tick^2**
> - **area di controllo: unità**

> dove un'unità si riferisce ad un nodo della strada e un tick equivale all'incirca ad un secondo.

### Direttive di base

- un automobilista altera la sua velocità in base al comportamento della macchina immediatamente successiva
  - cercherà di non tamponare la macchina successiva
  - cercherà di mantenere una **distanza minima**, raggiunta la quale cerca di mantenere la stessa velocità della macchina che precede. 
    - perciò se va troppo lento cercherà di accelerare, mentre se va troppo veloce cercherà di rallentare perchè siano rispettate la distanza minima e l'allineamento delle velocità
    - in altri termini cerca di massimizzare la distanza percorsa nell'unità di tempo e di minimizzare la distanza dalla macchina successiva (efficienza nel tempo e nello spazio)

![esempio 1](@attachment/ex1.gif)

### Schema di comportamento

Ad ogni iterazione ogni macchina:
1. determina se modificare la propria velocità,
2. si muove di un certo numero di caselle in base alla sua velocità.

#### Primo passo

Ad ogni iterazione ogni macchina confronta la velocità che ha con quella che avrà nell'iterazione successiva. Premettendo che $\Delta v = v_{f}-v_{0}$, si hanno cinque casi:
  - $\Delta v > 1$, significa che la velocità finale è molto più grande di quella iniziale, indica *accelerazione brusca*,
  - $\Delta v = 1$, significa che la velocità finale è di poco maggiore di quella iniziale, indica *accelerazione semplice*,
  - $\Delta v < -1$, significa che la velocità finale è molto più piccola di quella iniziale, indica *decelerazione brusca*
  - $\Delta v = -1$, significa che la velocità finale è di poco più inferiore a quella iniziale, indica *decelerazione semplice*,
  - $\Delta v = 0$, significa che la velocità finale e quella iniziale coincidono, perciò la velocità è costante e *l'accelerazione è nulla*.

> **Nota:** un caso particolare di decelazione brusca si ha quando la velocità finale è uguale a 0, in questo caso si ha una decelazione che altera lo stato della macchina e in particolare provoca l'arresto. Ciò si denota informalmente con il termine *inchiodata*. Quando invece la velocità iniziale è uguale a 0 e si ha $\Delta v > 1$ si parla di *sgommata*.

- Ogni automobilista ad ogni iterazione costruisce un insieme di scenari possibili basandosi sulla propria velocità, sulla capacità di accelerazione/decelerazione fornita dal suo veicolo e sulla distanza e la velocità che lo separa da entità ferme o in movimento (fine strada, semaforo, macchina successiva).

- La scelta dello scenario migliore avviene sfruttando un sistema di penalità, in altri termini il guidatore cerca di evitare frenate o accelerazioni brusche, inchiodate o sgommate e quando non può farne a meno cerca di scegliere la combinazione di mosse che gli permette di ridurre le penalità (manovre critiche di intensità minore meno penalizzanti e viceversa).

#### Secondo passo

A seguito della scelta dello scenario il guidatore attua i necessari cambiamenti e procede con l'azione di movimento.

