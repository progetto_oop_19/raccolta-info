---
pinned: true
title: Bozza cambio deadline
created: '2020-03-22T17:02:21.458Z'
modified: '2020-04-07T12:14:52.590Z'
---

# Bozza cambio deadline

Ci si concentra sui casi limite e quelli che meglio evidenziano il funzionamento degli algoritmi che si vuole implementare.

Abbiamo scelto la deadline B2 perché uno dei membri del gruppo non ha potuto partecipare attivamente alla realizzazione del progetto ed è rimasto molto indietro, in questo modo gli permettiamo di recuperare senza che si sacrifichi la possibilità di seguire le lezioni.
