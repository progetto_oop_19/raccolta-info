---
title: 'About Barriers, concurrency and simulations'
created: '2020-04-07T17:10:00.536Z'
modified: '2020-04-07T19:14:41.799Z'
---

# About Barriers, concurrency and simulations

From Java Concurrency in Practice, 116

---

Barriers are often used in simulations, where **the work to calculate one step can be done in parallel but all the work associated with a given step must complete before advancing to the next step**. 

For example, in n-body particle simulations, each step calculates an update to the position of each particle based on the lo- cations and other attributes of the other particles. 

Waiting on a barrier between each update ensures that all updates for step k have completed before moving on to step k + 1. 

CellularAutomata in Listing 5.15 demonstrates using a barrier to compute a cellular automata simulation, such as Conway’s Life game (Gardner, 1970). 

When parallelizing a simulation, it is generally impractical to assign a separate thread to each element (in the case of Life, a cell); this would require too many threads, and the overhead of coordinating them would dwarf the computation. 

Instead, it makes sense to partition the problem into a number of subparts, let each thread solve a subpart, and then merge the results. CellularAutomata partitions the board into Ncpu parts, where Ncpu is the number of CPUs available, and assigns each part to a thread.

At each step, the worker threads calculate new values for all the cells in their part of the board. When all worker threads have reached the barrier, the barrier action commits the new values to the data model. 

After the barrier action runs, the worker threads are released to compute the next step of the calculation, which includes consulting an isDone method to determine whether further iterations are required.

---

About `Runtime.getRuntime().availableProcessors()`
This value may change during a particular invocation of the virtual machine. Applications that are sensitive to the number of available processors should therefore occasionally poll this property and adjust their resource usage appropriately.
