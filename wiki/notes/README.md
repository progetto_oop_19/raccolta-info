---
title: README
created: '2020-02-24T15:49:29.649Z'
modified: '2020-03-24T11:25:31.177Z'
---

# README

[[Index|Index.md]]

---

# Traffic Simulator

Lorem ipsum dolor sit amet...

### Setup e lancio dell'applicazione

- Per prima cosa è necessario installare il JRE, può essere scaricato dal seguente link: [[https://java.com/en/download/]].

- Successivamente si deve scaricare il file .jar che si trova in questa repository, al seguente link: [[https://java.com/en/download/]]

- Per avviare l'applicazione basta lanciare:
```sh
$ java -jar traffic.jar 
```

### Librerie impiegate

Queste sono le librerie che abbiamo scelto di impiegare in questo progetto.

(JavaFx è moderno e permette di creare un'interfaccia più accattivamente anche se è meno portabile, a quanto pare, in ogni caso meglio mettere in buona luce le nostre scelte in qualche modo e non essere troppo laconici)

| Library | DOCS |
| ------ | ------ |
| JavaFX | [javaFx-doc] |



### Autori

* Adrian Lazar : https://bitbucket.org/
* Riccardo Bacca : https://bitbucket.org/
* Riccardo Battistini : https://bitbucket.org/
* Stefano Camporesi : https://bitbucket.org/

   [javaFx-doc]: link da inserire.


