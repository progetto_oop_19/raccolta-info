---
title: Git
created: '2020-02-24T14:47:03.776Z'
modified: '2020-03-24T11:25:31.597Z'
---

# Git

[[Index|Index.md]]

---

**Tavola dei contenuti**

1. configurazione iniziale
2. concetti di base
3. comandi utili 
4. best practices
5. procedure ricorrenti
6. workflow  (flusso di lavoro)

## 1. Configurazione iniziale di Git

- Impostare nome utente ed email per capire chi ha apportato le modifiche.

~~~sh
git config --global user.name "John Doe"
# conviene usare email istituzionale (secondo indicazioni dispense)
git config --global user.email johndoe@example.com
~~~

**NB:** si possono impostare username ed email personalizzati per ogni repository, usando i seguenti comandi:

~~~sh
# si elimina il flag --global
git config user.name "John Doe"
git config user.email johndoe@example.com
~~~

- Disabilitare la configurazione automatica dei caratteri che rappresentano una nuova linea di testo (il carattere di fine linea è diverso nei sistemi Unix e Unix-like e in Windows).

*A seconda del SO solo una delle seguenti opzioni*

~~~sh
# in Linux e MacOS basta usare il carattere presente nel testo
git config --global core.autocrlf input
# oppure
# in Windows bisogna disattivare la conversione automatica a CLRF
git config --global core.autocrlf false 
~~~

- Impostare un editor predefinito per Git, che sarà aperto per esempio se non si specifica un messaggio di commit o se si tenta un rebase, ecc.

~~~sh
git config --global core.editor "editorcommand"
~~~

  Dove `editorcommand` può essere, tra i tanti, emacs, vim, nano o su Windows notepad. Volendo anche Atom o Visual Studio Code.

-  Se Git, nel tentare di clonare una repository, dà come errore che la lunghezza del nome del file è troppo lunga, si può risolvere con il seguente comando:

~~~sh
git config --global core.longpaths true.
~~~

---

## 2. Concetti di base

- **Repository**

Il repository è l’insieme dei file che vengono tracciati dal DVCS assieme ai metadati, ossia alle informazioni che servono a ricostruire qualunque stato precedente.

---

- **Tracciamento delle differenze**

Abilità di registrare le differenze fra diverse versioni di uno o più file. Invece di salvare l’intero stato (tutto il contenuto di un file), vengono salvate solo le informazioni necessarie a ricostruire il file a partire dal salvataggio precedente.

**NB:** In un progetto Java vanno tracciati (aggiunti al tracker):
  - I sorgenti
  - Le risorse (icone, file di configurazione...)
  - Librerie jar copiate nel progetti (in realtà, vedrete in altri corsi,esistono sistemi migliori)
  - Eventuali file esterni, ad esempio un README.md, un file per la licenza, il file .project di Eclipse per facilitare l’import, ecc.
  - il file .gitignore

  *non* vanno tracciati:
  - I binari (rigenerabile dai sorgenti)
  - La documentazione javadoc (rigenerabile dai sorgenti)
  - I jar della vostra applicazione (rigenerabili)

---

- **Rimozione e rinominamento di file**

Git è in grado di capire da solo quando qualcosa è stato modificato o eliminato.
- Git tratta tutte le modifiche allo stesso modo
- A fronte della cancellazione di un file, basta aggiungerlo alla staging area perché la modifica venga registrata al successivo commit
- A fronte di una rinominazione, si aggiungono sia il file col vecchio nome che quello col nuovo
  - Diversamente, verrà trattata come una rimozione o un’aggiunta, a seconda di quale delle due modifiche aggiungete all’area di staging.

---

- **Commit**

Salvataggio dello stato del repository, oppure *snapshot della configurazione della staging area*.

Consiste nel creare un punto di salvataggio, che registri i nostri progressi e al quale potremo sempre
tornare. Assieme al salvataggio, vogliamo registrare alcuni **metadati**:

- Nome dell’autore del commit
- Informazioni per identificare e contattare l’autore (email)
- Un messaggio che riassuma quali modifiche sono state fatte in quel commit
- Data e ora del commit (acquisite automaticamente)
- Un identificativo univoco (hash) (generato automaticamente)

Il commit non salverà lo stato del progetto, ma il set di modifiche necessarie per portare i file dallo stato precedente a quello del nuovo commit. 

---

- **Staging area**

Insieme delle modifiche accodate per esser salvate al prossimo commit. Il processo di salvataggio si articola infatti in due fasi:

1. Staging — Selezione di quali file modificati, aggiunti o rimossi salvare al prossimo commit

2. Commit — Effettivo salvataggio delle modifiche presenti nella staging area

Nota: Mercurial differisce da Git in questo particolare. Il concetto di staging area in Mercurial è assente,lo stato corrente del repository è esso stesso lo stage.

---

- **Navigazione della storia**

Possibilità di tornare ad un qualunque commit(salvataggio) precedente o successivo

---

## 3. Buone e cattive pratiche

### Buone pratiche di commit

È molto importante specificare un messaggio di commit sensato.
  - Deve essere un breve riassunto di quanto è stato fatto dal salvataggio precedente.
  - Chi vede la storia del progetto deve capire immediatamente quali siano le differenze che il commit introduce.
  - **È buona norma scrivere in inglese usando il simple present**.

È molto importante fare commit piccoli e frequenti.
  - Idealmente, uno ad ogni modifica di cui sia possibile fornire una descrizione organica nel commit message.
  - Non importa se le modifiche sono minimali.

### Cattive pratiche di commit

- Messaggi non chiari, generici e/o troppo brevi, ad esempio:
  - Fix bug
  - Fix project
  - Add files
  - Commit

- Commit giganteschi con molte modifiche, magari non strettamente correlate fra loro.

---

## 4. Comandi utili

### Branching e merging



### Visualizzare l'elenco dei commit effettuati

Permette di vedere inoltre chi ha effettuato un commit quando e il relativo messaggio di commit.

~~~
git log
# oppure
git log --graph
~~~

### Come creare un punto di salvataggio 

Il sottocomando commit crea, a partire da ciò che si trova nella staging area,  il salvataggio

~~~sh
git commit -m "A message"
~~~

- Esegue un commit di tutte le modifiche aggiunte alla staging area
- Utilizza "A message" come commit message

~~~
git commit FILE1 FILE2 FILEN -m "A message"
~~~

- Esegue un commit salvando tutte le modifiche ai file elencati
- Utilizza "A message" come commit message

**NB:**
- Non è possibile non specificare un messaggio, se il messaggio viene lasciato vuoto, il commit viene rigettato
- Se l’opzione -m viene omessa, viene aperto un editor per l’inserimento del messaggio

---

### Come eliminare un punto di salvataggio

Si impiega il comando `git rebase`.

- Funziona senza effetti collaterali solo non è stato fatto il push del commit da eliminare, ovvero se il commit va eliminato solo dalla repository locale e non da quella centrale.
- Se è stato fatto il push del commit (o il commit era incluso in una push che si è fatta) si inquina la storia della repository sia ufficiale che locale (un disastro).

  Per approfondimenti consultare: [Insidie del rebasing](https://git-scm.com/book/en/v2/Git-Branching-Rebasing#_rebase_peril).

```
git rebase -i HEAD~2
```
apre l'editor di testo predefinito, con gli ultimi 2 commit dando una schermata di questo tipo:
(In generale: pick HASH-DEL-COMMIT NOME-COMMIT)
```
pick d9f1cf5 Changed again
pick 46cd867 Changed with mistake

# Rebase 105fd3d..46cd867 onto 105fd3d
#
# Commands:
#  p, pick = use commit
#  r, reword = use commit, but edit the commit message
#  e, edit = use commit, but stop for amending
#  s, squash = use commit, but meld into previous commit
#  f, fixup = like "squash", but discard this commit's log message
#  x, exec = run command (the rest of the line) using shell
#
# If you remove a line here THAT COMMIT WILL BE LOST.
# However, if you remove everything, the rebase will be aborted.
#
```

Per cancellare il commit e pulire la storia basta cancellare la riga 2, `pick 46cd867 Changed with mistake` (in generale pick HASH-DEL-COMMIT NOME-COMMIT-ERRATO). In questo caso non è necessario usare nessuno dei comandi indicati nei commenti sotto.

In seguito lanciare il comando `git status` darà il seguente messaggio di errore:

"Your branch is based on 'origin/master', but the upstream is gone."

Indica che:...
Può essere risolto con...

```
git branch --unset-upstream
```

In seguito per salvare i cambiamenti nella repository centrale:

```
git push --set-upstream origin master
```

Questo perché...

---

### Come aggiungere un file alla staging area

~~~sh
git add PATH TO FILE
~~~

---

### Come rimuovere un file dalla staging area

Il sottocomando reset rimuove delle modifiche dalla staging area (non dai files, a meno di non specificare apposite opzioni)

~~~sh
git reset PATH TO FILE
# si possono anche indicare più file, in questo modo:
git reset PATH TO FILE 1 PATH TO FILE 2 PATH TO FILE N
~~~

- **Rimuove dalla staging area, non dal tracking,** le modifiche fatte al file indicato
- Il file potrebbe anche non esistere, ad esempio se si cancella un file e si aggiunge la modifica alla staging area.

~~~sh
git rm PATH TO FILE
~~~

- Rimuove il file indicato dalla staging area e dal tracking, richiede di forzare l'eliminazione se il file ha subito modifiche dall'ultimo commit.

---

### Come vedere i cambiamenti apportati

Se non si trovano nella staging area:

~~~sh
git diff
~~~

Se si trovano nella staging area:

~~~sh
git diff --staged # oppure git diff --cached
~~~

In questo caso il comando esegue un confronto tra ciò che c'è nella directory in cui si trova la repository e ciò che si trova nella staging area.

Permette di vedere i cambiamenti apportati **staged**,rispetto all'ultimo commit.

Il comando `git log --all` permette di vedere tutto il log dei commit anche se si è nello stato *detached head*.

---

### Come aggiungere un file al tracking

- Basta crearlo nella cartella in cui si trova la repository.

---

### Come rimuovere un file dal tracking

- Con il comando `git rm PATH TO FILE`.

---

### Come aggiungere ulteriori cambiamenti all'ultimo commit senza riscrivere la storia

- Funziona solo se i nuovi cambiamenti da inglobare nell'ultimo commit non sono stati inseriti in un altro commit nel frattempo,
- Funziona solo nella repository locale, se nel frattempo si è lanciato `git push` è meglio lasciar stare perché si rischia di riscrivere la storia ufficiale (pericoloso).

~~~sh
git add NUOVICAMBIAMENTI
git commit --amend
~~~

Dopo l'esecuzione di questo cambiamento:

~~~sh
MESSAGGIOULTIMOCOMMIT
# Please enter the commit message for your changes. Lines starting
# with ‘#’ will be ignored, and an empty message aborts the commit.
#
# Author: John Doe <john@doe.com># Date: Thu Apr 21 22:40:30 2016 -0300
#
# On branch some_branch# Your branch is up-to-date with ‘origin/some_branch’.
#
# Changes to be committed:
# modified: NUOVICAMBIAMENTI
~~~

Si può modificare il MESSAGGIOULTIMOCOMMIT, salvare il file e chiudere l'editor.

Nel caso non si volesse modificare il messaggio dell'ultimo commit si può lanciare direttamente:

~~~sh
git commit --amend -no-edit
~~~

**NB:** se si vuole semplicemente cambiare il messaggio dell'ultimo commit si può lanciare:

~~~sh
git commit --amend -m"NUOVOMESSAGGIODICOMMIT"
# oppure se si vuole aprire l'editor
git commit --amend
~~~

---

## 5. Procedure ricorrenti

### Per apportare modifiche alla repository centrale a partire da quella locale

**La prima volta** è necessario clonare la repository centrale per creare quella locale.

~~~sh
git clone https://NOMEUTENTE@bitbucket.org/progetto_oop_19/prova.git

git add Wiki/
git commit -m"aggiunto wiki di base"

git add Wireframes/
git commit -m"aggiuntoi progetti GUI di base"

git push
~~~

In seguito bastano questi comandi.

~~~sh
git add file_di_prova.txt
git commit -m"aggiunta file di prova"

git pull
git push
~~~

**NB:** prima di ogni modifica è meglio verificare lo stato della repository con `git status`.

**NB:** se si lavora con una repository locale creata clonando quella centrale con il protocollo HTTPS ad ogni interazione con bitbucket sarà richiesta una password, se si impiegano chiavi SSH non è invece necessario. Tuttavia per impiegare le chiavi SSH è necessario un setup iniziale.

Dalle impostazioni di bitbucket:
> SSH keys
Use [[SSH|https://confluence.atlassian.com/bitbucket/ssh-keys-935365775.html]] to avoid password prompts when you push code to Bitbucket. Learn how to [[generate an SSH key|https://confluence.atlassian.com/bitbucket/set-up-an-ssh-key-728138079.html]]. 

### Per poter aggiornare la repository locale a seguito di cambiamenti in quella centrale.

~~~sh
git pull
~~~

---

## 6. Workflow

- Un membro del gruppo fa da repo mantainer, ovvero manterrà la repository centrale su Bitbucket.
- Gli altri membri clonano il repository centrale e creano una copia locale su cui lavorare.
- Ciascuno lavora parallemente sul proprio repository locale e condivide tramite `push` e `pull` il proprio lavoro con gli altri.

