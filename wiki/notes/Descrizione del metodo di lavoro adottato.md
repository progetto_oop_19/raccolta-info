---
tags: [Relazione]
title: Descrizione del metodo di lavoro adottato
created: '2020-04-16T09:53:06.210Z'
modified: '2020-04-16T10:38:44.698Z'
---

# Descrizione del metodo di lavoro adottato

![image](@attachment/image.png)

### Git

Inizialmente abbiamo lavorato su un unico branch, il **master**, e solo nel **main**, impiegando esclusivamente pull e push per scambiarci il codice scritto.

In seguito abbiamo creato un nuovo branch, **develop**, in cui si inseriscono, nel **main**, tutte le classi non ancora completate o che potrebbero essere scartate prima di arrivare nel branch master.

La sezione **test** sul branch **develop** è impiegata per sperimentare: ad esempio riguardo l'uso di nuove strutture dati e la verifica del funzionamento di una o più classi insieme. (come il playground)

Infine la sezione **test** del branch **master** sarà impiegata per inserire i test junit delle classi ufficiali/ultimate.
