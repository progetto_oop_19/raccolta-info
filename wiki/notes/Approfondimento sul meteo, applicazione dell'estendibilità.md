---
title: 'Approfondimento sul meteo, applicazione dell''estendibilità'
created: '2020-02-29T11:06:18.489Z'
modified: '2020-03-24T11:24:23.940Z'
---

# Approfondimento sul meteo, applicazione dell'estendibilità

Creazione di un modulo che amplia le capacità degli automobilisti. **Raggio d'azione** che definisce quando è possibile applicare le regole (non a qualsiasi distanza dalle altre macchine)

**Nebbia**, singoli banchi o a livello di scenario influenzano il raggio d'azione degli automobilisti.

(simula l'abbassamento della velocità media)

-> possibilità di definire nel builder se questa capacità è presente

-> inserimento tramite interfaccia e relativa implementazione a cui le macchine abilitate accedono per alterare il loro comportamento.

> **Nota:** si tratta di modificare il comportamento a livello di accesso a interfacce già esistenti non di aggiungere altro (per quanto riguarda il raggio d'azione) perciò forse si dovrebbe semplicemente creare un nuovo set di regole e caricarlo.

Si sposta il problema sul set di regole.
