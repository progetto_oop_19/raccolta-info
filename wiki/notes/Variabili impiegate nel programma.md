---
title: Variabili impiegate nel programma
created: '2020-02-24T15:41:48.295Z'
modified: '2020-03-24T11:25:30.879Z'
---

## Variabili impiegate nel programma

[[Index|Index.md]]

---
(this.velocity < 0) ? this.velocity = 0 : this.velocity = this.velocity

Tratto da: 
- [*Introduction to the Theory of Cellular Automata and One-Dimensional Traffic Simulation* by Richard Cochinos](https://theory.org/complexity/traffic/)
- []()

**NB:** i valori di queste variabili sono memorizzati in appositi file, si potrebbero gestire come *enum*?

### Variabili legati a statistiche e debugging 

`CARS-OVERALL` indica il numero totale di macchine nello scenario in un dato istante di tempo.

`CARS-EASTBOUND` indica il numero totale di macchine che vanno in direzione Est (oppure che provengono da Ovest) nello scenario in un dato istante di tempo.

`CARS-NORTHBOUND` indica il numero totale di macchine che vanno in direzione Nord (oppure che provengono da Sud) nello scenario in un dato istante di tempo.

---

`WAIT-TIME-OVERALL` mostra quante macchine si trovano nello stato di attesa, ovvero quante macchine sono ferme in coda in un dato istante di tempo.

`WAIT-TIME-EASTBOUND` (in attesa in direzione EST) mostra quante macchine, tra quelle che vanno in direzione Est, sono in attesa in un dato istante di tempo.

`WAIT-TIME-NORTHBOUND` (in attesa in direzione NORD)
mostra quante macchine, tra quelle che vanno in direzione Nord, sono in attesa in un dato istante di tempo.

---

`CLOCK` mostra quanti istanti di tempo sono trascorsi, in altri termini il numero di iterazioni dall'avvio dello scenario. (si resetta con l'apposito tasto)

---

### Variabili relative al *Pannello di configurazione*

Permettono all'utente di modificare il comportamento dell'AC - per ora sono alcuni slider.

`FREQ-EAST` indica quanto spesso viaggiano nuove macchine sulla strada verso Est.

`FREQ-NORD` indica quanto spesso viaggiano nuove macchine sulla strada verso Nord.

---

`VEL-MAX` indica quanto velocemente possono viaggiare le macchine.

`MAX-ACC`

`MAX-BRAKE`

---

`GREEN-LENGHT` determina quanto a lungo la luce del semaforo rimarrà verde.

`YELLOW-LENGHT` determina quanto a lungo la luce del semaforo rimarrà gialla.

~~`RED-LENGHT`[^1] determina quanto a lungo la luce del semaforo rimarrà rossa.~~

---

### Variabili relative al *Pannello di gestione del tempo*

`VAI/STOP` indica se si deve aggiornare la variabile `CLOCK` al trascorrere della durata di un'iterazione. (in altri termini se dopo un secondo trascorso nella realtà si deve alterare il valore di clock)

`RESET` indica se si deve resettare o meno la variabile `CLOCK`

`VAI-UNO` al termine di un'iterazione la simulazione si ferma automaticamente. (utile per lo studio della simulazione, non inserito nel wireframe)


[^1]: per ragioni che ancora non comprendo questo valore nella simulazione originale non può essere alterato, forse ha a che fare con la gestione dei thread (serve un tempo minimo altrimenti potrebbero verificarsi malfunzionamenti). Andrebbe considerata come una questione in sospeso da chiarire.
