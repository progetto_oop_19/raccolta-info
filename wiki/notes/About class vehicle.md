---
title: About class vehicle
created: '2020-04-08T07:27:37.516Z'
modified: '2020-04-08T12:55:11.096Z'
---

# About class vehicle

Lo stato è definito da una serie di campi:

velocità
accelerazione
area di controllo

pos corrente
pos iniziale
pos arrivo

stato

DriverBehaviour
VehiclePrimitives

Un oggetto veicolo deve poter essere creato solo dal suo builder (static nested class)

Un oggetto veicolo deve esporre solo dei metodi per effettuare l'update della sua posizione in base ad altri parametri e algoritmi al resto del modello (protected).

~~~java
public interface Vehicle {

  update();

}

~~~

Come distinguere tra classi che implementano operazioni primitive e di alto-livello?

Gli algoritmi usano metodi che non si trovano in Vehicle e devono stare in VehiclePrimitives.

Parallelismo multi-core, programmazione multi-threaded

terminazione di un thread

organizzazione task-oriented, individuazione e gestione dipendenze tra task
meccanismi per la coordinazione fra thread, task e sotto-task
