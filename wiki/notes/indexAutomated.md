---
pinned: true
title: indexAutomated
created: '2020-03-24T11:14:32.907Z'
modified: '2020-03-24T11:26:05.271Z'
---

# Project tree

.
 * [Index.md](./Index.md)
 * [Configurazione locale di Eclipse.md](./Configurazione locale di Eclipse.md)
 * [Convezioni nella scrittura del codice.md](./Convezioni nella scrittura del codice.md)
 * [Directory del progetto (2).md](./Directory del progetto (2).md)
 * [Gradle.md](./Gradle.md)
 * [Logger e debugging.md](./Logger e debugging.md)
 * [Pannello statistiche.md](./Pannello statistiche.md)
 * [Panoramica.md](./Panoramica.md)
 * [Qualità del codice.md](./Qualità del codice.md)
 * [Qualità del software.md](./Qualità del software.md)
 * [Raccolte di codice utile al progetto.md](./Raccolte di codice utile al progetto.md)
 * [README.md](./README.md)
 * [Ripartizione dei compiti.md](./Ripartizione dei compiti.md)
 * [Salvataggio dati su file.md](./Salvataggio dati su file.md)
 * [Strumenti, librerie e fonti impiegate.md](./Strumenti, librerie e fonti impiegate.md)
 * [Studiare il modello.md](./Studiare il modello.md)
 * [Sull'Automa Cellulare e TrafficSimulator.md](./Sull'Automa Cellulare e TrafficSimulator.md)
 * [Gerarchie di classi e builder.md](./Gerarchie di classi e builder.md)
 * [Approfondimento sul meteo, applicazione dell'estendibilità.md](./Approfondimento sul meteo, applicazione dell'estendibilità.md)
 * [Basi di partenza.md](./Basi di partenza.md)
 * [Git.md](./Git.md)
 * [Punti di discussione.md](./Punti di discussione.md)
 * [Capitolo 1 - Analisi.md](./Capitolo 1 - Analisi.md)
 * [Componenti JavaFx.md](./Componenti JavaFx.md)
 * [Pannello controllo del tempo.md](./Pannello controllo del tempo.md)
 * [Concetti di base.md](./Concetti di base.md)
 * [DESIGN.odt](./DESIGN.odt)
 * [Capitolo 2 - Design.md](./Capitolo 2 - Design.md)
 * [Forum.txt](./Forum.txt)
 * [passi progetto.pdf](./passi progetto.pdf)
 * [Design progetto.md](./Design progetto.md)
 * [Variabili impiegate nel programma.md](./Variabili impiegate nel programma.md)
 * [Esempi notevoli.md](./Esempi notevoli.md)
 * [Tavola dei contenuti.md](./Tavola dei contenuti.md)
 * [Algoritmi per funzionamento delle macchine.md](./Algoritmi per funzionamento delle macchine.md)
 * [trick.sh](./trick.sh)
 * [indexAutomated.md](./indexAutomated.md)
