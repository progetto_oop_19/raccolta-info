---
tags: [Relazione]
title: Utilità
created: '2020-04-16T08:41:02.424Z'
modified: '2020-04-16T08:52:20.486Z'
---

# Utilità

Per dire a log4j di usare il file di configurazioni in resources.

Senza non si può generare l'output del logger su file.

~~~java 
System.setProperty("log4j.configurationFile",
	    		    ClassLoader.getSystemResource("log4j2.yaml").getPath());
~~~

Permette di ottenere il numero di processori da usare per inizializzare l'executor.

~~~java
Runtime.getRuntime().availableProcessors());
~~~

Per aggiungere icona e titolo adeguati.

~~~java
stage.setTitle("Traffic Simulator");stage.getIcons().(ImageLoader.ICON.getImage());
~~~

La classe `ImageLoader` .
