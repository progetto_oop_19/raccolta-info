---
title: Gestione orientata ai processi
created: '2020-04-06T07:08:19.483Z'
modified: '2020-04-06T11:08:50.322Z'
---

# Gestione orientata ai processi

[Come create una eccezione personalizzata](https://stackify.com/java-custom-exceptions/)

Chapter 9 for sequential processing in GUIs

Logging framework

Executor framework

GUI framework (JavaFx)

Thread.pool config in chapter 8

## Executor service lifecycle

```mermaid
graph TD;
running-->shuttingDown;
shuttingDown-->terminated;

```

## Task lifecycle

```mermaid
graph TD;
created-->submitted;
submitted-->started;
started-->completed;

```

## Thread in threadpool lifecycle

```mermaid
graph TD;
requestTask-->executeTask;
executeTask-->waitTask;
waitTask-->requestTask;

```

---

Descrivere a parole il processo della simulazione

---

Gestire arresto di thread a seguito di input dell'utente, errori o chiusura dell'applicazione

---

*Tasks with time limit*

IDEA: Uso di task con limite temporale: algoritmo per gestire velocità dell'automobilista: se troppo lungo gli si mette a disposizione un tempo limitato

Alcuni oggetti:

- `VehicleGenerator`
- `VehicleSink`

Istanziati da lane?
Rappresentano dei task
Istanziati da un componente che viene richiamato con update periodicamente

`Road` deve comunicare col pubblico NON `Lane`

*Tasks with cancellation policy*

Devono essere task con supporto alla cancellazione tramite meccanismo cooperativo (chi chiede la cancellazione e il task che la esegue)

If you code your tasks to be responsive to interruption,you can use interruption as your cancellation mechanism and take advantage of the interruption support provided by many library classes.

---

Capacità di recuperare lo stato dell'applicazione dopo la chiusura

---

Using a FIFO queue like `LinkedBlockingQueue` or `ArrayBlockingQueue` causes tasks to be started in the order in which they arrived. For more con- trol over task execution order, you can use a `PriorityBlockingQueue`, which orders tasks according to priority. Priority can be defined by natural order (if tasks implement Comparable) or by a Comparator .

---

*Limita il numero di task in entrata*

There is no predefined saturation policy to makeexecute block when the work queue is full. However, the same effect can be accomplished by using a Semaphore to bound the task injection rate

---




